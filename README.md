# kylin-scanner
Kylin Scanner is an interface-friendly scanning software developed with Qt5.
The software can scan according to the resolution, size and color mode of
the scanning device itself. At the same time, It Increases post-processing of scanned pictures,
including one-click beautification, intelligent correction and text recognition.
Other image processing tips can also be reflected in this software, such as clipping, rotation, etc.

![Main Picture](docs/scanner.png)



## Function lists
- [x] Normal scanning(different device, type, resolution, size, color mode, formats)
- [x] One-clicked beautification
- [x] Intelligent correction
- [x] text recognition OCR
- [x] Image processing: clipping, rotation, watermarking, symmetry, etc
- [x] Send email
- [x] Save as multi-formats contents
- [x] Multiple scan and simple scan
- [x] Usb hotplug 

## How to build and install in Ubuntu Environments
1. Build and Install via **debuild**
``` bash
# Git clone
$ git clone http://gitlab2.kylin.com/kylin-desktop/kylin-scanner.git

# Go to root dir /kylin-scanner/ and build
$ cd kylin-scanner/

# build
$ debuild

# Use dpkg to install, the same as other architectures.
# Such as x86, loongarch64, mips, arch, and so on.

# kylin-scanner_1.0.0_amd64.deb is generated by debuild
$ sudo dpkg -i ../*.deb
$ sudo apt install -f # Handle depents, while it's the first install
$ sudo dpkg -i ../*.deb

# Enjoy it
$ kylin-scanner
or
open and search "kylin scanner" after enter Win key

# Handle image while no scanner, `xxx` is your image
$ KYLIN_SCANNER_IMAGE_DEBUG=1 kylin-scanner -f xxx --hide
```

2. Build and Install via **qmake**
``` bash
# Git clone
$ git clone http://gitlab2.kylin.com/kylin-desktop/kylin-scanner.git

# Go to root dir /kylin-scanner/
$ cd kylin-scanner/

# build
$ qmake
$ make

# install
$ sudo make install

```

## How to make helpful docs
```
$ sudo apt install doxygen graphviz 
$ doxygen -g
$ ./autodoxygen.sh
$ doxygen Doxygen
$ firefox docs/html/index.html
```

## Report bugs
Bugs should be report to the kylin-scanner bug tracking system: http://gitlab2.kylin.com/kylin-desktop/kylin-scanner/issues

