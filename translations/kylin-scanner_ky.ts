<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="48"/>
        <source>Detect scanners, please waiting</source>
        <translation>Сканерлерди аныктоо, сураныч, күтүү</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="65"/>
        <source>No available scan devices</source>
        <translation>Жеткиликтүү сканерлеу түзмөктөрү</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="69"/>
        <source>Connect</source>
        <translation>Туташуу</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="46"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="58"/>
        <source>Unable to read text, please retrey</source>
        <translation>Текстти окуй албай калды, кайра кайра</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="73"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>Сканерлерди туташтырыңыз, сканерлеуди баштоо үчүн сканерлеу баскычын басыңыз.</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new scanner to refresh the device list.</source>
        <translatorcomment>Эч кандай сканер табылган жок, аппарат тизмесин сергитүү үчүн жаңы сканер туташтыруу.</translatorcomment>
        <translation>Эч кандай сканер табылган жок, аппарат тизмесин сергитүү үчүн жаңы сканер туташтыруу.</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation type="vanished">麒麟扫描</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">Сканер</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="vanished">存在新设备连接，正在重新检测所有扫描设备，请稍等。</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>Single</source>
        <translation type="vanished">单页扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="434"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>Жараксыз аргумент, сураныч, аргументтерди өзгөртүү же башка сканерлерди которуу.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="497"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>Сканерлеу ишке ашпады, сураныч, сиздин сканер текшерүү же башка сканерлерди которуу. Эгер сканерди колдонууну улантууну кааласаңыз, параметрлерди чыкылдатып, аппаратты кайра иштетүү үчүн тизмени жаңыртыңыз.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>Alert</source>
        <translation>Эскертүү</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>A new Scanner has been connected.</source>
        <translation>Жаңы Сканнер туташтырылды.</translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="171"/>
        <source>Ok</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="466"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>Орнотмо менен алек, күткүлө же башка сканерлерди алмаштырыңыз.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="470"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>Документтерден азыктандыруучуну документтештириңиз, кагаздарды жайгаштырыңыз жана кайрадан сканерлеуиңиз.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="474"/>
        <source>Scan operation has been cancelled.</source>
        <translation>Сканерлеу операциясы жокко чыгарылган.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="478"/>
        <source>Scan failed, operation is not supported.</source>
        <translation>Сканерлеу ишке ашпады, операция колдолбойт.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="481"/>
        <source>Scan failed, Document fedder jammed.</source>
        <translation>Сканерлеу ишке ашпады, Документ федерал тыгылып калды.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="484"/>
        <source>Scan failed, Error during device I/O.</source>
        <translation>Сканерлеу ишке ашпады, I/O орнотмосунун учурундагы ката.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="487"/>
        <source>Scan failed, Out of memory.</source>
        <translation>Сканерлеу ишке ашпады, эс тутумдан чыгып.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="490"/>
        <source>Scan failed, Access to resource has been denied.</source>
        <translation>Scan failed, Access to resource has been denied.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="493"/>
        <source>Scan failed, Scanner cover is open.</source>
        <translation>Сканерлеу ишке ашпады, Сканер мукабасы ачык.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="544"/>
        <source>System sleep/sleep detected. To ensure normal use of the scanner, please click on restart to restart the scanner application. If you want to continue operating the scanned image, click cancel, but the scanning related functions will be disabled and will take effect after restarting.</source>
        <translation>Система уйку / уйку аныкталды. Сканердин нормалдуу колдонулушун камсыз кылуу үчүн, сканердик тиркемени кайра иштетүү үчүн кайра иштетүүнү басыңыз. Эгер сканерленген сүрөттү иштетүүнү улантууну кааласаңыз, басыңыз жокко чыгарыңыз, бирок сканерлеу менен байланышкан функциялар өчүрүлөт жана кайра иштеткенден кийин күчүнө кирет.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="549"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="550"/>
        <source>Restart</source>
        <translation>Кайра иштетүү</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="620"/>
        <source>Running beauty ...</source>
        <translation>Чуркап сулуулук ...</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="641"/>
        <source>Running rectify ...</source>
        <translation>Чуркоо оңдоп ...</translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="43"/>
        <source>Scanner not detected</source>
        <translation>Сканер табылган жок</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="58"/>
        <location filename="../src/sendmail.cpp" line="81"/>
        <location filename="../src/sendmail.cpp" line="82"/>
        <source>No email client</source>
        <translation>Электрондук почта кардар жок</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="65"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="93"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>Системада электрондук почта кардар табылбады, сураныч, биринчи кезекте электрондук почта кардар орнотуу.</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="107"/>
        <source>Install</source>
        <translation>Орнотуу</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="105"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="161"/>
        <location filename="../src/scandialog.cpp" line="46"/>
        <location filename="../src/scansettingswidget.cpp" line="1359"/>
        <location filename="../src/mainwidget.cpp" line="70"/>
        <location filename="../src/mainwidget.cpp" line="169"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <location filename="../src/saneobject.cpp" line="327"/>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>ADF Duplex</source>
        <translation>АДФ Дуплекс</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="581"/>
        <source>Fail to open the scanner</source>
        <translation>Сканерди ачуу мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2017"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="212"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>Multiple</source>
        <translation>Көп</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="213"/>
        <source>Flatbed</source>
        <translation>Жалпак түбүндө</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="59"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>Сергитүү тизмеси. Сураныч, сергитерлик ийгилик тууралуу маалыматты күткүлө...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Scanner is on detecting...</source>
        <translation>Сканер аныктоо боюнча...</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <source>Single</source>
        <translation>Бойдок</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">Суроо</translation>
    </message>
    <message>
        <source>Current </source>
        <translation type="vanished">Учурдагы</translation>
    </message>
    <message>
        <source> User</source>
        <translation type="vanished">Колдонуучу</translation>
    </message>
    <message>
        <source> has already opened kylin-scanner, open will close </source>
        <translation type="vanished"> буга чейин кайлин-сканер ачылган, ачык жабылат</translation>
    </message>
    <message>
        <source>&apos;s operations. Are you continue?</source>
        <translation type="vanished">&quot;Операциялар. Сиз улантуудасызбы?</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="652"/>
        <source>Color</source>
        <translation>Түс</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="659"/>
        <source>Gray</source>
        <translation>Грей</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="665"/>
        <source>Lineart</source>
        <translation>Лайнерт</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="730"/>
        <source>Default Type</source>
        <translation>Дефолт түрү</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="809"/>
        <source>Flatbed</source>
        <translation>Жалпак түбүндө</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="819"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="828"/>
        <source>ADF Front</source>
        <translation>АДФ фронту</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="837"/>
        <source>ADF Back</source>
        <translation>АДФ артка</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="846"/>
        <source>ADF Duplex</source>
        <translation>АДФ Дуплекс</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="896"/>
        <location filename="../src/saneobject.cpp" line="2195"/>
        <source>4800 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="899"/>
        <location filename="../src/saneobject.cpp" line="2199"/>
        <source>2400 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="902"/>
        <location filename="../src/saneobject.cpp" line="2203"/>
        <source>1200 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="905"/>
        <location filename="../src/saneobject.cpp" line="2207"/>
        <source>600 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="908"/>
        <location filename="../src/saneobject.cpp" line="2211"/>
        <source>300 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="911"/>
        <location filename="../src/saneobject.cpp" line="2215"/>
        <source>200 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="914"/>
        <location filename="../src/saneobject.cpp" line="2219"/>
        <source>150 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="917"/>
        <location filename="../src/saneobject.cpp" line="2223"/>
        <source>100 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="920"/>
        <location filename="../src/saneobject.cpp" line="2227"/>
        <source>75 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="938"/>
        <location filename="../src/saneobject.cpp" line="2243"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../src/device/ukui_apt.cpp" line="79"/>
        <location filename="../src/device/ukui_apt.cpp" line="160"/>
        <source>Install timeout.</source>
        <translation>Орнотуу күтүү убакыты.</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="1943"/>
        <source>Default Type</source>
        <translation>Дефолт түрү</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1935"/>
        <source>Flatbed</source>
        <translation>Жалпак түбүндө</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="416"/>
        <source>Refresh list complete.</source>
        <translation>Сергитүү тизмеси аяктады.</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1937"/>
        <source>ADF</source>
        <translation>АДФ</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1939"/>
        <source>ADF Front</source>
        <translation>АДФ фронту</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1941"/>
        <source>ADF Back</source>
        <translation>АДФ артка</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1945"/>
        <source>ADF Duplex</source>
        <translation>АДФ Дуплекс</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2021"/>
        <source>75 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2023"/>
        <source>100 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2025"/>
        <source>150 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2027"/>
        <source>200 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2029"/>
        <source>300 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2031"/>
        <source>600 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2033"/>
        <source>1200 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2035"/>
        <source>2400 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2037"/>
        <source>4800 dpi</source>
        <translation></translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="61"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="65"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="85"/>
        <location filename="../src/scandialog.cpp" line="184"/>
        <source>Number of pages scanning: </source>
        <translation>Барактарды сканерлеу саны: </translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="97"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>Сканерлеуди жокко чыгаруу,бир аз күткүлө!</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="182"/>
        <source>Multiple</source>
        <translation>Көп</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="754"/>
        <source>Scanner device</source>
        <translation>Сканер түзмөк</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>File settings</source>
        <translation>Файл параметрлери</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="586"/>
        <source>Device</source>
        <translation>Түзмөк</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="128"/>
        <source>Select a directory</source>
        <translation>Каталогду тандоо</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="151"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>Учурда колдонуучунун каталогду өзгөртүүгө уруксаты жок </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="214"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>Жалпак сканерлеу режими бир нече сканерлеуди колдобойт.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="219"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Multiple</source>
        <translation>Көп</translation>
    </message>
    <message>
        <source>This resolution will take a long time to scan, please choose carelly.</source>
        <translation type="vanished">Бул резолюция сканерлеу үчүн көп убакыт талап кылынат, сураныч, камкордук тандап алыңыз.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="535"/>
        <source>Alert</source>
        <translation>Эскертүү</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="587"/>
        <source>Pages</source>
        <translation>Барактар</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="588"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="282"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Color</source>
        <translation>Түс</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="590"/>
        <source>Resolution</source>
        <translation>Резолюция</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="591"/>
        <source>Size</source>
        <translation>Көлөмү</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="592"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="593"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="762"/>
        <source>scanner01</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="594"/>
        <source>Save</source>
        <translation>Сактоо</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="602"/>
        <location filename="../src/scansettingswidget.cpp" line="603"/>
        <source>Mail to</source>
        <translation>Почта</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="781"/>
        <location filename="../src/scansettingswidget.cpp" line="1142"/>
        <location filename="../src/scansettingswidget.cpp" line="1143"/>
        <source>Save as</source>
        <translation>Үнөмдөө</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="216"/>
        <location filename="../src/scansettingswidget.cpp" line="240"/>
        <location filename="../src/scansettingswidget.cpp" line="271"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Single</source>
        <translation>Бойдок</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="296"/>
        <source>4800 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="297"/>
        <source>2400 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="298"/>
        <source>1200 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="300"/>
        <source>This resolution will take a long time to scan, please choose carefully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="328"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>&apos;/&apos; мүнөзүн камтый албайт.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="333"/>
        <location filename="../src/scansettingswidget.cpp" line="470"/>
        <source>cannot save as hidden file.</source>
        <translation>жашыруун файл катары сактай албайт.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="459"/>
        <source>Save As</source>
        <translation>Куткаруу</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="483"/>
        <source>Path without access rights: </source>
        <translation>Жеткиликтүүлүк укуктары жок жол: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="487"/>
        <source>File path that does not exist: </source>
        <translation>Жок файл жолу: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="533"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation> буга чейин бар, сиз аны ашыкча жазууну каалайсызбы? Эгер сиз көп беттүү сканерлеуди аткарып жатсаңыз, анда ал бир нече файлдын ашыкча жазылышына себеп болушу мүмкүн. Сураныч, сак болгула!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="731"/>
        <source>Start Scan</source>
        <translation>Сканерлеуди баштоо</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1133"/>
        <location filename="../src/scansettingswidget.cpp" line="1134"/>
        <source>Store text</source>
        <translation>Текстти сактоо</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="532"/>
        <source>The file </source>
        <translation>Файл </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="vanished">已存在，您想覆盖它吗？</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">кеңештер</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="589"/>
        <source>Colour</source>
        <translation>Түс</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="270"/>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>Flatbed</source>
        <translation>Жалпак түбүндө</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>ADF</source>
        <translation>АДФ</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Gray</source>
        <translation>Грей</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="284"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Lineart</source>
        <translation>Лайнерт</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>75 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>100 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>150 dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1047"/>
        <source>Resolution is empty!</source>
        <translation>Чечим бош!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A5</source>
        <translation></translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">Сканер</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1362"/>
        <source>Yes</source>
        <translation>Сканер</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="194"/>
        <location filename="../src/sendmail.cpp" line="218"/>
        <source>Select email client</source>
        <translation>Электрондук почта кардар тандоо</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="200"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="203"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="232"/>
        <location filename="../src/sendmail.cpp" line="233"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="236"/>
        <location filename="../src/sendmail.cpp" line="237"/>
        <source>Confirm</source>
        <translation>Ырастоосу</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="64"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="72"/>
        <source>Ok</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1113"/>
        <source>Canceling...Please waiting...</source>
        <translation>Жокко чыгаруу... Сураныч, күтүү...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1124"/>
        <source>Running beauty ...</source>
        <translation>Чуркап сулуулук ...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1157"/>
        <source>Running rectify ...</source>
        <translation>Чуркоо оңдоп ...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>Форма ...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="194"/>
        <location filename="../src/titlebar/titlebar.h" line="82"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>Option</source>
        <translation>Опция</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="179"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="220"/>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="261"/>
        <location filename="../src/titlebar/titlebar.cpp" line="145"/>
        <source>Maximize</source>
        <translation>Максималдуу</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="308"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="53"/>
        <source>Refresh List</source>
        <translation>Сергитүү тизмеси</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="70"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="74"/>
        <source>About</source>
        <translation>Жөнүндө</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="76"/>
        <source>Version: </source>
        <translation>Версиясы: </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="77"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>Билдирүү ЛАН тексттик чат жана файл которуу милдеттерин берет. Сервер куруунун зарылдыгы жок. Ал бир эле учурда өз ара аракеттенүү жана параллель жөнөтүү жана алуу үчүн бир нече адамды колдойт.</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="87"/>
        <source>Exit</source>
        <translation>Чыгуу</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="142"/>
        <source>Restore</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="192"/>
        <source>The current file is not saved. Do you want to save it?</source>
        <translation>Учурдагы файл сакталбайт. Сиз аны сактап келет?</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="vanished">当前文档未保存，是否保存？</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="196"/>
        <source>Straight &amp;Exit</source>
        <translation>Түз &amp;, чыгуу</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="197"/>
        <source>&amp;Save Exit</source>
        <translation>Чыгууну сактоо</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="74"/>
        <location filename="../src/toolbarwidget.cpp" line="168"/>
        <source>Beauty</source>
        <translation>Сулуулук</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="80"/>
        <location filename="../src/toolbarwidget.cpp" line="174"/>
        <source>Rectify</source>
        <translation>Оңдоо</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="86"/>
        <location filename="../src/toolbarwidget.cpp" line="180"/>
        <source>OCR</source>
        <translation>ОКР</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="94"/>
        <location filename="../src/toolbarwidget.cpp" line="188"/>
        <source>Crop</source>
        <translation>Өсүмдүк</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="100"/>
        <location filename="../src/toolbarwidget.cpp" line="194"/>
        <source>Rotate</source>
        <translation>Айлантуу</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="106"/>
        <location filename="../src/toolbarwidget.cpp" line="200"/>
        <source>Mirror</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="112"/>
        <location filename="../src/toolbarwidget.cpp" line="206"/>
        <source>Watermark</source>
        <translation>Ватермарк</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="131"/>
        <location filename="../src/toolbarwidget.cpp" line="225"/>
        <source>ZoomIn</source>
        <translation>Зоомин</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="125"/>
        <location filename="../src/toolbarwidget.cpp" line="219"/>
        <source>ZoomOut</source>
        <translation>Чоңойтуу</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation type="vanished">扫描仪已断开连接！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="95"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>Сканердик аппаратты сурап алуу. Сураныч, күтүү...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="119"/>
        <source>New Scanner has been Connected.</source>
        <translation>Жаңы Сканер туташтырылган.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="129"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>Сканер ажыратылат. Эгер сканерди өчүрүү сканерлеуде болсо, Сканерди басыңыз же сканердин ката тууралуу билдирүүнү күткүлө.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="135"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>Сканер ажыратуу,сергитерлик сканер тизмеси болуп саналат. Сураныч, күтүү...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="150"/>
        <source>Scanner list refresh complete.</source>
        <translation>Сканер тизмеси сергитүү аяктады.</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="38"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="50"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="62"/>
        <source>Searching for scanner...</source>
        <translation>Сканер издөө...</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="47"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="66"/>
        <source>Add watermark</source>
        <translation>Суу белгисин кошуу</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="85"/>
        <source>Confirm</source>
        <translation>Ырастоосу</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="185"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>Файлды ачуу </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="186"/>
        <source>Filename</source>
        <translation>Файл аты</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="190"/>
        <source>Hide scan settings widget</source>
        <translation>Сканерлеу параметрлерин виджетти жашыруу</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Symbol</source>
        <translation>Символ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>Alert</source>
        <translation>Эскертүү</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <source>You need to manually confirm whether the printer devices included in the list are scanning and printing all-in-one machines. If they are not scanning and printing all-in-one machines, ignore them.</source>
        <translation>Тизмеге кирген принтер түзмөктөрү бардык машиналарды сканерлеп, басып чыгарып жаткан-жатпаганын кол менен тастыктоо керек. Эгерде алар бардык машиналарды сканерлеп, басып чыгарбаса, аларды четке каккыла.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="71"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="84"/>
        <source>Device List</source>
        <translation>Түзмөк тизмеси</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="97"/>
        <location filename="../src/newdevicelistpage.cpp" line="117"/>
        <location filename="../src/newdevicelistpage.cpp" line="318"/>
        <location filename="../src/newdevicelistpage.cpp" line="375"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="98"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="118"/>
        <source>Before</source>
        <translation>Мурун</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="119"/>
        <source>Install</source>
        <translation>Орнотуу</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="197"/>
        <source>Scanner</source>
        <translation>Сканер</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="199"/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="216"/>
        <source>Device Name:</source>
        <translation>Орнотмо аты:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="220"/>
        <source>Driver Name:</source>
        <translation>Драйвердин аты:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <source>This driver is from third party, may cause some unmetable result.</source>
        <translation>Бул айдоочу үчүнчү тараптан болуп саналат, кандайдыр бир тынымсыз натыйжага алып келиши мүмкүн.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>This driver is provided by the manufacturer. Please contact the corresponding manufacturer of the device to obtain the driver program.</source>
        <translation>Бул драйвер өндүрүүчү тарабынан берилет. Драйвердик программаны алуу үчүн аппараттын тиешелүү өндүрүүчүсү менен байланышыңыздар.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="312"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>Жеткиликтүү айдоочулар жок, сиз кол менен айдоочуларды кошууну каалайсызбы?</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="316"/>
        <source>Add</source>
        <translation>Кошуу</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="353"/>
        <location filename="../src/newdevicelistpage.cpp" line="458"/>
        <source>Installing driver...</source>
        <translation>Драйверди орнотуу...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="370"/>
        <source>Installation successful. Do you want to use this scanner now? Click &apos;Use&apos; to reset udev (administrator password required) and restart the scanning application to refresh the device list. If you still want to continue operating the scanned image, you will need to manually restart and use the scanning function.</source>
        <translation>Орнотуу ийгиликтүү. Сиз азыр бул сканер колдонууну каалайсызбы? Удевти (администратор сырсөз талап кылынат) калыбына келтирүү үчүн &apos;колдонууну&quot; басыңыз жана аппарат тизмесин жаңыртуу үчүн сканерлеу тиркемесин кайра иштетиңиз. Эгерде сиз дагы эле сканерленген сүрөттү колдонууну улантууну кааласаңыз, анда кол менен кайра баштоо жана сканерлеу милдетин колдонуу зарыл болот.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="376"/>
        <source>Use</source>
        <translation>Колдонуу</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="407"/>
        <source>Installation failed.</source>
        <translation>Орнотуу ишке ашпады.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="410"/>
        <source>Ok</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="452"/>
        <source>Select a directory</source>
        <translation>Каталогду тандоо</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="48"/>
        <location filename="../src/showocrwidget.cpp" line="157"/>
        <location filename="../src/showocrwidget.cpp" line="165"/>
        <source>The document is in character recognition ...</source>
        <translation>Документ мүнөзү таанылууда ...</translation>
    </message>
</context>
</TS>
