<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="48"/>
        <source>Detect scanners, please waiting</source>
        <translation>كشف الماسحات الضوئية ، يرجى الانتظار</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="65"/>
        <source>No available scan devices</source>
        <translation>لا توجد أجهزة فحص متوفرة</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="69"/>
        <source>Connect</source>
        <translation>عشق</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="46"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="58"/>
        <source>Unable to read text, please retrey</source>
        <translation>غير قادر على قراءة النص ، يرجى إعادة الرجوع</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>حوار</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="73"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>قم بتوصيل الماسحات الضوئية ، يرجى النقر فوق زر المسح لبدء المسح.</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new scanner to refresh the device list.</source>
        <translatorcomment>未检测到扫描仪，请插入新的扫描仪以刷新设备列表。</translatorcomment>
        <translation>لم يتم اكتشاف ماسح ضوئي ، قم بتوصيل ماسح ضوئي جديد لتحديث قائمة الأجهزة.</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation type="vanished">麒麟扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="70"/>
        <location filename="../src/mainwidget.cpp" line="170"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="vanished">存在新设备连接，正在重新检测所有扫描设备，请稍等。</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>Single</source>
        <translation type="vanished">单页扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="474"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>وسيطة غير صالحة ، يرجى تغيير الوسيطات أو تبديل الماسحات الضوئية الأخرى.</translation>
    </message>
    <message>
        <source>error code:</source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="537"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>فشل الفحص ، يرجى التحقق من الماسح الضوئي أو تبديل الماسحات الضوئية الأخرى. إذا كنت تريد الاستمرار في استخدام الماسح الضوئي، فانقر فوق خيارات، قم بتحديث القائمة لإعادة تشغيل الجهاز.</translation>
    </message>
    <message>
        <source>Detected user manual hibernation. Please check the scanner status. If scanning does not continue, you need to manually click cancel or refresh the list to resume device scanning function.</source>
        <translation type="vanished">检测到用户手动休眠。请检查扫描仪状态。如果扫描无法继续，则需要手动单击“取消”或刷新列表以恢复设备扫描功能。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="619"/>
        <source>Alert</source>
        <translation>تنبيه</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="619"/>
        <source>A new Scanner has been connected.</source>
        <translation>تم توصيل ماسح ضوئي جديد.</translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="172"/>
        <source>Ok</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="506"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>الجهاز مشغول ، يرجى الانتظار أو تبديل الماسحات الضوئية الأخرى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="510"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>تغذية المستندات من المستندات ، يرجى وضع الأوراق ومسحها ضوئيا مرة أخرى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="514"/>
        <source>Scan operation has been cancelled.</source>
        <translation>تم إلغاء عملية الفحص.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="518"/>
        <source>Scan failed, operation is not supported.</source>
        <translation>فشل الفحص، العملية غير مدعومة.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="521"/>
        <source>Scan failed, Document fedder jammed.</source>
        <translation>فشل الفحص، انحشار فيدر المستندات.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="524"/>
        <source>Scan failed, Error during device I/O.</source>
        <translation>فشل الفحص ، خطأ أثناء إدخال / إخراج الجهاز.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="527"/>
        <source>Scan failed, Out of memory.</source>
        <translation>فشل الفحص، نفاد الذاكرة.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="530"/>
        <source>Scan failed, Access to resource has been denied.</source>
        <translation>فشل الفحص، تم رفض الوصول إلى المورد.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="533"/>
        <source>Scan failed, Scanner cover is open.</source>
        <translation>فشل الفحص ، غطاء الماسح الضوئي مفتوح.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="584"/>
        <source>System sleep/sleep detected. To ensure normal use of the scanner, please click on restart to restart the scanner application. If you want to continue operating the scanned image, click cancel, but the scanning related functions will be disabled and will take effect after restarting.</source>
        <translation>تم اكتشاف سكون / نوم النظام. لضمان الاستخدام العادي للماسح الضوئي ، يرجى النقر فوق إعادة التشغيل لإعادة تشغيل تطبيق الماسح الضوئي. إذا كنت ترغب في متابعة تشغيل الصورة الممسوحة ضوئيا، فانقر فوق إلغاء، ولكن سيتم تعطيل الوظائف المتعلقة بالمسح الضوئي وستصبح سارية المفعول بعد إعادة التشغيل.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="589"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="590"/>
        <source>Restart</source>
        <translation>اعاده تشغيل</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="660"/>
        <source>Running beauty ...</source>
        <translation>جمال الجري ...</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="682"/>
        <source>Running rectify ...</source>
        <translation>تشغيل التصحيح ...</translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="43"/>
        <source>Scanner not detected</source>
        <translation>لم يتم اكتشاف الماسح الضوئي</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="54"/>
        <location filename="../src/sendmail.cpp" line="77"/>
        <location filename="../src/sendmail.cpp" line="78"/>
        <source>No email client</source>
        <translation>لا يوجد عميل بريد إلكتروني</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="61"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="89"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>لا تجد عميل البريد الإلكتروني في النظام ، يرجى تثبيت عميل البريد الإلكتروني أولا.</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="103"/>
        <source>Install</source>
        <translation>أقام</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="101"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="176"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="235"/>
        <location filename="../src/saneobject.cpp" line="327"/>
        <location filename="../src/saneobject.cpp" line="450"/>
        <location filename="../src/saneobject.cpp" line="511"/>
        <location filename="../src/scansettingswidget.cpp" line="531"/>
        <location filename="../src/scansettingswidget.cpp" line="553"/>
        <source>ADF Duplex</source>
        <translation>وحدة تغذية المستندات التلقائية على الوجهين</translation>
    </message>
    <message>
        <source>Fail to open the scanner, error code </source>
        <translation type="vanished">打开扫描仪失败，错误代码</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="588"/>
        <source>Fail to open the scanner</source>
        <translation>فشل في فتح الماسح الضوئي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2085"/>
        <source>Auto</source>
        <translation>تلقائي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="450"/>
        <location filename="../src/saneobject.cpp" line="511"/>
        <location filename="../src/scansettingswidget.cpp" line="210"/>
        <location filename="../src/scansettingswidget.cpp" line="531"/>
        <location filename="../src/scansettingswidget.cpp" line="553"/>
        <source>Multiple</source>
        <translation>ضعف</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="211"/>
        <source>Flatbed</source>
        <translation>مسطحه</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="59"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>قائمة تحديث. يرجى انتظار معلومات نجاح التحديث ...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Scanner is on detecting...</source>
        <translation>الماسح الضوئي قيد الكشف عن ...</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="235"/>
        <source>Single</source>
        <translation>واحد</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="287"/>
        <source>Question</source>
        <translation>سؤال</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source>Current </source>
        <translation>حالي </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source> User</source>
        <translation> مستخدم</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="285"/>
        <source> has already opened kylin-scanner, open will close </source>
        <translation> لقد فتح بالفعل kylin-scanner ، سيغلق الفتح </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="286"/>
        <source>&apos;s operations. Are you continue?</source>
        <translation>العمليات. هل تستمر؟</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="660"/>
        <source>Color</source>
        <translation>لون</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="667"/>
        <source>Gray</source>
        <translation>رمادي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="673"/>
        <source>Lineart</source>
        <translation>لاين آرت</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="738"/>
        <source>Default Type</source>
        <translation>النوع الافتراضي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="817"/>
        <source>Flatbed</source>
        <translation>مسطحه</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="827"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="836"/>
        <source>ADF Front</source>
        <translation>ADF الأمامي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="845"/>
        <source>ADF Back</source>
        <translation>ADF الخلفي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="854"/>
        <source>ADF Duplex</source>
        <translation>وحدة تغذية المستندات التلقائية على الوجهين</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="904"/>
        <location filename="../src/saneobject.cpp" line="968"/>
        <source>4800 dpi</source>
        <translation>4800 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="907"/>
        <location filename="../src/saneobject.cpp" line="972"/>
        <source>2400 dpi</source>
        <translation>2400 نقطة لكل بوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="910"/>
        <location filename="../src/saneobject.cpp" line="976"/>
        <source>1200 dpi</source>
        <translation>1200 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="913"/>
        <location filename="../src/saneobject.cpp" line="980"/>
        <source>600 dpi</source>
        <translation>600 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="916"/>
        <location filename="../src/saneobject.cpp" line="984"/>
        <source>300 dpi</source>
        <translation>300 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="919"/>
        <location filename="../src/saneobject.cpp" line="988"/>
        <source>200 dpi</source>
        <translation>200 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="922"/>
        <location filename="../src/saneobject.cpp" line="992"/>
        <source>150 dpi</source>
        <translation>150 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="925"/>
        <location filename="../src/saneobject.cpp" line="996"/>
        <source>100 dpi</source>
        <translation>100 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="928"/>
        <location filename="../src/saneobject.cpp" line="1000"/>
        <source>75 dpi</source>
        <translation>75 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="946"/>
        <location filename="../src/saneobject.cpp" line="1016"/>
        <source>Auto</source>
        <translation>تلقائي</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <location filename="../src/runningdialog.cpp" line="141"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <location filename="../src/runningdialog.cpp" line="170"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="2011"/>
        <source>Default Type</source>
        <translation>النوع الافتراضي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2003"/>
        <source>Flatbed</source>
        <translation>مسطحه</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="416"/>
        <source>Refresh list complete.</source>
        <translation>اكتمل تحديث القائمة.</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2005"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2007"/>
        <source>ADF Front</source>
        <translation>ADF الأمامي</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2009"/>
        <source>ADF Back</source>
        <translation>ADF الخلفي</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2013"/>
        <source>ADF Duplex</source>
        <translation>وحدة تغذية المستندات التلقائية على الوجهين</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2089"/>
        <source>75 dpi</source>
        <translation>75 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2091"/>
        <source>100 dpi</source>
        <translation>100 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2093"/>
        <source>150 dpi</source>
        <translation>150 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2095"/>
        <source>200 dpi</source>
        <translation>200 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2097"/>
        <source>300 dpi</source>
        <translation>300 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2099"/>
        <source>600 dpi</source>
        <translation>600 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2101"/>
        <source>1200 dpi</source>
        <translation>1200 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2103"/>
        <source>2400 dpi</source>
        <translation>2400 نقطة لكل بوصة</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2105"/>
        <source>4800 dpi</source>
        <translation>4800 نقطة في البوصة</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="47"/>
        <location filename="../src/scandialog.cpp" line="62"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="66"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="86"/>
        <location filename="../src/scandialog.cpp" line="184"/>
        <source>Number of pages scanning: </source>
        <translation>عدد الصفحات التي يتم مسحها ضوئيا: </translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="98"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>إلغاء الفحص ، يرجى الانتظار لحظة!</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="182"/>
        <source>Multiple</source>
        <translation>ضعف</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="769"/>
        <source>Scanner device</source>
        <translation>جهاز الماسحة الضوئية</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="774"/>
        <source>File settings</source>
        <translation>إعدادات الملف</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="601"/>
        <source>Device</source>
        <translation>جهاز</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="126"/>
        <source>Select a directory</source>
        <translation>حدد دليلا</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="149"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>حاليا لا يمتلك المستخدم أي إذن لتعديل الدليل </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="212"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>لا يدعم وضع المسح الضوئي المسطح المسح الضوئي المتعدد.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="217"/>
        <location filename="../src/scansettingswidget.cpp" line="967"/>
        <source>Multiple</source>
        <translation>ضعف</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="299"/>
        <source>This resolution will take a long time to scan, please choose carelly.</source>
        <translation>سيستغرق هذا القرار وقتا طويلا للمسح الضوئي ، يرجى الاختيار بعناية.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="300"/>
        <source>Alert</source>
        <translation>تنبيه</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="602"/>
        <source>Pages</source>
        <translation>الصفحات</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="603"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="281"/>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Color</source>
        <translation>لون</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="605"/>
        <source>Resolution</source>
        <translation>دقة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="606"/>
        <source>Size</source>
        <translation>حجم</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="607"/>
        <source>Format</source>
        <translation>تنسيق</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="608"/>
        <source>Name</source>
        <translation>اسم</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="777"/>
        <source>scanner01</source>
        <translation>الماسح الضوئي01</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="609"/>
        <source>Save</source>
        <translation>أنقذ</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="617"/>
        <location filename="../src/scansettingswidget.cpp" line="618"/>
        <source>Mail to</source>
        <translation>البريد إلى</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="796"/>
        <location filename="../src/scansettingswidget.cpp" line="1148"/>
        <location filename="../src/scansettingswidget.cpp" line="1149"/>
        <source>Save as</source>
        <translation>حفظ باسم</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="214"/>
        <location filename="../src/scansettingswidget.cpp" line="238"/>
        <location filename="../src/scansettingswidget.cpp" line="270"/>
        <location filename="../src/scansettingswidget.cpp" line="967"/>
        <source>Single</source>
        <translation>واحد</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="295"/>
        <source>4800 dpi</source>
        <translation>4800 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="296"/>
        <source>2400 dpi</source>
        <translation>2400 نقطة لكل بوصة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="297"/>
        <source>1200 dpi</source>
        <translation>1200 نقطة في البوصة</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="vanished">该分辨率将会花费很长时间扫描，请谨慎选择！</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="327"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>لا يمكن أن تحتوي على حرف &quot;/&quot;.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="332"/>
        <location filename="../src/scansettingswidget.cpp" line="472"/>
        <source>cannot save as hidden file.</source>
        <translation>لا يمكن حفظه كملف مخفي.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="461"/>
        <source>Save As</source>
        <translation>حفظ باسم</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="485"/>
        <source>Path without access rights: </source>
        <translation>المسار بدون حقوق الوصول: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="489"/>
        <source>File path that does not exist: </source>
        <translation>مسار الملف غير الموجود: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="548"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation> موجود بالفعل ، هل تريد الكتابة فوقه؟ إذا كنت تقوم بإجراء فحص متعدد الصفحات، فقد يتسبب ذلك في الكتابة فوق ملفات متعددة. من فضلك كن حذرا!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="746"/>
        <source>Start Scan</source>
        <translation>بدء المسح</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1139"/>
        <location filename="../src/scansettingswidget.cpp" line="1140"/>
        <source>Store text</source>
        <translation>تخزين النص</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="547"/>
        <source>The file </source>
        <translation>الملف </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="vanished">已存在，您想覆盖它吗？</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="550"/>
        <source>tips</source>
        <translation>النصائح</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="604"/>
        <source>Colour</source>
        <translation>لون</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="269"/>
        <location filename="../src/scansettingswidget.cpp" line="984"/>
        <source>Flatbed</source>
        <translation>مسطحه</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="984"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Gray</source>
        <translation>رمادي</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="283"/>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Lineart</source>
        <translation>لاين آرت</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>75 dpi</source>
        <translation>75 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>100 dpi</source>
        <translation>100 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>150 dpi</source>
        <translation>150 نقطة في البوصة</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1053"/>
        <source>Resolution is empty!</source>
        <translation>القرار فارغ!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1068"/>
        <source>A4</source>
        <translation>أ 4</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1068"/>
        <source>A5</source>
        <translation>أ 5</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1365"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1368"/>
        <source>Yes</source>
        <translation>نعم</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="198"/>
        <location filename="../src/sendmail.cpp" line="222"/>
        <source>Select email client</source>
        <translation>حدد عميل البريد الإلكتروني</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="204"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="207"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="236"/>
        <location filename="../src/sendmail.cpp" line="237"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="240"/>
        <location filename="../src/sendmail.cpp" line="241"/>
        <source>Confirm</source>
        <translation>أكد</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="66"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="74"/>
        <source>Ok</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1148"/>
        <source>Canceling...Please waiting...</source>
        <translation>الغاء... من فضلك انتظر...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1159"/>
        <source>Running beauty ...</source>
        <translation>جمال الجري ...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1192"/>
        <source>Running rectify ...</source>
        <translation>تشغيل التصحيح ...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>شكل</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>الماسح الضوئي kylin</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="194"/>
        <location filename="../src/titlebar/titlebar.h" line="83"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="173"/>
        <source>Option</source>
        <translation>خيار</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="217"/>
        <source>Minimize</source>
        <translation>تصغير</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="258"/>
        <location filename="../src/titlebar/titlebar.cpp" line="145"/>
        <source>Maximize</source>
        <translation>تعظيم</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="305"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="53"/>
        <source>Refresh List</source>
        <translation>قائمة التحديث</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="70"/>
        <source>Help</source>
        <translation>تعليمات</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="74"/>
        <source>About</source>
        <translation>عن</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="76"/>
        <source>Version: </source>
        <translation>الإصدار: </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="77"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>يوفر Message وظائف الدردشة النصية ونقل الملفات في شبكة LAN. ليست هناك حاجة لبناء خادم. وهو يدعم عدة أشخاص للتفاعل في نفس الوقت والإرسال والاستقبال بالتوازي.</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="87"/>
        <source>Exit</source>
        <translation>مخرج</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="142"/>
        <source>Restore</source>
        <translation>يستعيد</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="192"/>
        <source>The current file is not saved. Do you want to save it?</source>
        <translation>لم يتم حفظ الملف الحالي. هل تريد حفظه؟</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="vanished">当前文档未保存，是否保存？</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="196"/>
        <source>Straight &amp;Exit</source>
        <translation>مستقيم &amp; خروج</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="197"/>
        <source>&amp;Save Exit</source>
        <translation>&amp;حفظ الخروج</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="64"/>
        <location filename="../src/toolbarwidget.cpp" line="158"/>
        <source>Beauty</source>
        <translation>جمال</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="70"/>
        <location filename="../src/toolbarwidget.cpp" line="164"/>
        <source>Rectify</source>
        <translation>عدل</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="76"/>
        <location filename="../src/toolbarwidget.cpp" line="170"/>
        <source>OCR</source>
        <translation>التعرف الضوئي على الحروف</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="84"/>
        <location filename="../src/toolbarwidget.cpp" line="178"/>
        <source>Crop</source>
        <translation>محصول</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="90"/>
        <location filename="../src/toolbarwidget.cpp" line="184"/>
        <source>Rotate</source>
        <translation>دور</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="96"/>
        <location filename="../src/toolbarwidget.cpp" line="190"/>
        <source>Mirror</source>
        <translation>مرآة</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="102"/>
        <location filename="../src/toolbarwidget.cpp" line="196"/>
        <source>Watermark</source>
        <translation>العلامه المائيه</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="121"/>
        <location filename="../src/toolbarwidget.cpp" line="215"/>
        <source>ZoomIn</source>
        <translation>تكبير</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="115"/>
        <location filename="../src/toolbarwidget.cpp" line="209"/>
        <source>ZoomOut</source>
        <translation>تصغير</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation type="vanished">扫描仪已断开连接！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="95"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>الاستعلام عن جهاز الماسح الضوئي. من فضلك انتظر ...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="119"/>
        <source>New Scanner has been Connected.</source>
        <translation>تم توصيل ماسح ضوئي جديد.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="129"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>تم فصل الماسح الضوئي. إذا قمت بفصل الماسح الضوئي قيد الفحص، فانقر فوق إلغاء الفحص أو انتظر حتى يقوم الماسح الضوئي بالإبلاغ عن رسالة خطأ.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="135"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>الماسح الضوئي هو قطع الاتصال ، تحديث قائمة الماسح الضوئي. من فضلك انتظر ...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="150"/>
        <source>Scanner list refresh complete.</source>
        <translation>اكتمل تحديث قائمة الماسح الضوئي.</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="46"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="59"/>
        <source>Searching for scanner...</source>
        <translation>البحث عن ماسح ضوئي...</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="46"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="54"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="65"/>
        <source>Add watermark</source>
        <translation>إضافة علامة مائية</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="81"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>أكد</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="200"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>افتح الملف &lt;filename&gt;</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Filename</source>
        <translation>اسم الملف</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="205"/>
        <source>Hide scan settings widget</source>
        <translation>إخفاء أداة إعدادات الفحص</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Name</source>
        <translation>اسم</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Symbol</source>
        <translation>رمز</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <location filename="../src/newdevicelistpage.cpp" line="255"/>
        <location filename="../src/newdevicelistpage.cpp" line="259"/>
        <source>Alert</source>
        <translation>تنبيه</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <source>You need to manually confirm whether the printer devices included in the list are scanning and printing all-in-one machines. If they are not scanning and printing all-in-one machines, ignore them.</source>
        <translation>تحتاج إلى التأكد يدويا مما إذا كانت أجهزة الطابعة المضمنة في القائمة تقوم بمسح وطباعة الأجهزة المتكاملة في واحد. إذا لم يقوموا بمسح الأجهزة المتكاملة وطباعتها ، فتجاهلها.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="71"/>
        <source>Close</source>
        <translation>غلق</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="84"/>
        <source>Device List</source>
        <translation>قائمة الأجهزة</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="97"/>
        <location filename="../src/newdevicelistpage.cpp" line="117"/>
        <location filename="../src/newdevicelistpage.cpp" line="309"/>
        <location filename="../src/newdevicelistpage.cpp" line="366"/>
        <source>Cancel</source>
        <translation>إلغاء الأمر</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="98"/>
        <source>Next</source>
        <translation>مقبل</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="118"/>
        <source>Before</source>
        <translation>قبل</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="119"/>
        <source>Install</source>
        <translation>أقام</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="189"/>
        <source>Scanner</source>
        <translation>ماسح ضوئي</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="191"/>
        <source>Printer</source>
        <translation>طابعة</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="208"/>
        <source>Device Name:</source>
        <translation>اسم الجهاز:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="212"/>
        <source>Driver Name:</source>
        <translation>اسم السائق:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="255"/>
        <source>This driver is from third party, may cause some unmetable result.</source>
        <translation>هذا السائق من طرف ثالث ، وقد يتسبب في بعض النتائج التي لا يمكن مواجهتها.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="259"/>
        <source>This driver is provided by the manufacturer. Please contact the corresponding manufacturer of the device to obtain the driver program.</source>
        <translation>يتم توفير برنامج التشغيل هذا من قبل الشركة المصنعة. يرجى الاتصال بالشركة المصنعة المقابلة للجهاز للحصول على برنامج التشغيل.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="303"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>لا توجد برامج تشغيل متوفرة ، هل تريد إضافة برامج تشغيل يدويا؟</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="307"/>
        <source>Add</source>
        <translation>جمع</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="344"/>
        <location filename="../src/newdevicelistpage.cpp" line="449"/>
        <source>Installing driver...</source>
        <translation>تثبيت برنامج التشغيل...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="361"/>
        <source>Installation successful. Do you want to use this scanner now? Click &apos;Use&apos; to reset udev (administrator password required) and restart the scanning application to refresh the device list. If you still want to continue operating the scanned image, you will need to manually restart and use the scanning function.</source>
        <translation>تم التثبيت بنجاح. هل تريد استخدام هذا الماسح الضوئي الآن؟ انقر فوق &quot;استخدام&quot; لإعادة تعيين udev (كلمة مرور المسؤول مطلوبة) وأعد تشغيل تطبيق المسح لتحديث قائمة الأجهزة. إذا كنت لا تزال ترغب في متابعة تشغيل الصورة الممسوحة ضوئيا ، فستحتاج إلى إعادة التشغيل يدويا واستخدام وظيفة المسح.</translation>
    </message>
    <message>
        <source>Installation successful. Do you want to use this scanner now?</source>
        <translation type="vanished">安装成功。你想现在使用这个扫描仪吗？</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="367"/>
        <source>Use</source>
        <translation>استخدام</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="398"/>
        <source>Installation failed.</source>
        <translation>فشل التثبيت.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="401"/>
        <source>Ok</source>
        <translation>موافق</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="443"/>
        <source>Select a directory</source>
        <translation>حدد دليلا</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="48"/>
        <location filename="../src/showocrwidget.cpp" line="157"/>
        <location filename="../src/showocrwidget.cpp" line="165"/>
        <source>The document is in character recognition ...</source>
        <translation>المستند في التعرف على الأحرف ...</translation>
    </message>
</context>
</TS>
