<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="48"/>
        <source>Detect scanners, please waiting</source>
        <translation>سېكاننېرلاش ئاپپاراتىنى تەكشۈرۈپ كۆرۈڭ، كۈتۈپ تۇرۇڭ ...</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="65"/>
        <source>No available scan devices</source>
        <translation>ئىشلىتىشكە بولىدىغان سكاننېرلاش ئۈسكۈنىسى يوق</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="69"/>
        <source>Connect</source>
        <translation>ئۇلاش</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="46"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="58"/>
        <source>Unable to read text, please retrey</source>
        <translation>مەن تېكىستنى ئوقۇيالمايدىمەن، قايتا سىناڭ.</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ رامكىسى</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="73"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>سىكاننېرلاشنى ئۇلاڭ، سىكاننېرلاشنى باشلاش ئۈچۈن سىكاننېرلاش كۇنۇپكىسىنى چېكىڭ.</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new scanner to refresh the device list.</source>
        <translatorcomment>未检测到扫描仪，请插入新的扫描仪以刷新设备列表。</translatorcomment>
        <translation>سكاننېرنى تەكشۈرمىگەن، يېڭى سكاننېرنى چېتىپ ئۈسكۈنە تىزىملىكىنى يېڭىلايدۇ.</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation type="vanished">麒麟扫描</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">سكاننېر</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="vanished">存在新设备连接，正在重新检测所有扫描设备，请稍等。</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>Single</source>
        <translation type="vanished">单页扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="434"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>ئىناۋەتسىز مۇلاھىزىلەر، دەلىل-ئىسپاتلارنى ئۆزگەرتىڭ ياكى باشقا سكاننېرلارنى ئالماشتۇرۇڭ.</translation>
    </message>
    <message>
        <source>error code:</source>
        <translation type="vanished">خاتالىق كودى:</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="497"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>سكاننېرلاش مەغلۇپ بولدى، سكاننېرنى تەكشۈرۈپ بېقىڭ ياكى باشقا سكاننېرنى ئالماشتۇرۇڭ. ئەگەر سكاننېرنى داۋاملىق ئىشلىتىشنى ئويلىسىڭىز، «تاللاش» نى چېكىپ، تىزىملىكنى يېڭىلاپ ئۈسكۈنىنى قايتا قوزغىتىڭ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>Alert</source>
        <translation>ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>A new Scanner has been connected.</source>
        <translation>يېڭى بىر سكاننېر ئۇلاندى.</translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="171"/>
        <source>Ok</source>
        <translation>ماقۇلبولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="466"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>ئۈسكۈنە ئالدىراش، ساقلاپ تۇرۇڭ ياكى باشقا سكاننېرلارنى ئالماشتۇرۇڭ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="470"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>ھۆججەت بىلەن تەمىنلىگۈچىنى ھۆججەتتىن چىقىرىۋەتسىڭىز، ھۆججەتلەرنى قويۇپ قايتا سىكاننېرلاڭ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="474"/>
        <source>Scan operation has been cancelled.</source>
        <translation>سىكاننېرلاش مەشغۇلاتى ئەمەلدىن قالدۇرۇلدى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="478"/>
        <source>Scan failed, operation is not supported.</source>
        <translation>سكاننېرلاش مەغلۇپ بولدى، مەشغۇلات قوللىمىدى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="481"/>
        <source>Scan failed, Document fedder jammed.</source>
        <translation>سكاننېرلىشى مەغلۇپ بولدى، ھۆججەت Fedder توختاپ قالدى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="484"/>
        <source>Scan failed, Error during device I/O.</source>
        <translation>سكاننېرلىغاندا مەغلۇپ بولدى، قۇرۇلما I/O جەريانىدا خاتالىق كۆرۈلدى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="487"/>
        <source>Scan failed, Out of memory.</source>
        <translation>سكاننېرلىغاندا مەغلۇپ بولدى، ئەستە ساقلىغۇسى يوق.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="490"/>
        <source>Scan failed, Access to resource has been denied.</source>
        <translation>سكاننېرلاش مەغلۇپ بولدى، بايلىقنى زىيارەت قىلىش رەت قىلىندى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="493"/>
        <source>Scan failed, Scanner cover is open.</source>
        <translation>سكاننېرلاش مەغلۇپ بولدى، سكاننېرنىڭ قاپقىقى ئېچىلىپ بولدى.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="544"/>
        <source>System sleep/sleep detected. To ensure normal use of the scanner, please click on restart to restart the scanner application. If you want to continue operating the scanned image, click cancel, but the scanning related functions will be disabled and will take effect after restarting.</source>
        <translation>سىستېما ئۇخلاش، ئۇخلاش تەكشۈرۈلدى. سكاننېرنىڭ نورمال ئىشلىتىلىشىگە كاپالەتلىك قىلىش ئۈچۈن، قايتا قوزغىتىشنى چېكىپ سكاننېرنىڭ قوللىنىشىنى قايتا قوزغىتىڭلار. ئەگەر سىكاننېرلاپ ئىشلىتىلىدىغان سۈرەتنى داۋاملىق مەشغۇلات قىلماقچى بولسىڭىز، بىراق سىكاننېرلاپ ئىشلىتىشكە ئالاقىدار ئىقتىدارلار توختىلىپ، قايتا قوزغىغاندىن كېيىن كۈچكە ئىگە بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="549"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="550"/>
        <source>Restart</source>
        <translation>قايتا قوزغىتىش</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="620"/>
        <source>Running beauty ...</source>
        <translation>يۈگرەيدىغان ساھىبجامال ...</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="641"/>
        <source>Running rectify ...</source>
        <translation>يۈگۈرۈشنى تەرتىپكە سېلىش ...</translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="43"/>
        <source>Scanner not detected</source>
        <translation>قايتا قوزغىتىش</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="58"/>
        <location filename="../src/sendmail.cpp" line="81"/>
        <location filename="../src/sendmail.cpp" line="82"/>
        <source>No email client</source>
        <translation>ئېلخەت خېرىدارى يوق</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="65"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="93"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>سىستېمىدا ئېلخەت خېرىدارىنى تاپالماي، ئاۋۋال ئېلخەت خېرىدارىنى قاچىلاڭ.</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="107"/>
        <source>Install</source>
        <translation>قاچىلاش</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="105"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="161"/>
        <location filename="../src/scandialog.cpp" line="46"/>
        <location filename="../src/scansettingswidget.cpp" line="1359"/>
        <location filename="../src/mainwidget.cpp" line="70"/>
        <location filename="../src/mainwidget.cpp" line="169"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <location filename="../src/saneobject.cpp" line="327"/>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>ADF Duplex</source>
        <translation>ADF Duplex</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="581"/>
        <source>Fail to open the scanner</source>
        <translation>سكاننېر ئاچالمىدى</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2017"/>
        <source>Auto</source>
        <translation>ئاپتۇماتىك</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="212"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>Multiple</source>
        <translation>كۆپ</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="213"/>
        <source>Flatbed</source>
        <translation>تۈزلەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="59"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>يېڭىلىق يارىتىش تىزىملىكى. يېڭىلىنىش مۇۋەپپەقىيەت ئۇچۇرىنى ساقلاڭ...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Scanner is on detecting...</source>
        <translation>سكاننېر تەكشۈرۈۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <source>Single</source>
        <translation>يەككە تەنھا</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">مەسىلە</translation>
    </message>
    <message>
        <source>Current </source>
        <translation type="vanished">نۆۋەتتىكى </translation>
    </message>
    <message>
        <source> User</source>
        <translation type="vanished"> ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source> has already opened kylin-scanner, open will close </source>
        <translation type="vanished"> ئاللىقاچان Kylin-scanner نى ئېچىپ بولدى، open نى تاقايدۇ </translation>
    </message>
    <message>
        <source>&apos;s operations. Are you continue?</source>
        <translation type="vanished">&apos;نىڭ مەشغۇلاتى. سىز داۋاملاشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="652"/>
        <source>Color</source>
        <translation>رەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="659"/>
        <source>Gray</source>
        <translation>كۈلرەڭ</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="665"/>
        <source>Lineart</source>
        <translation>Lineart</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="730"/>
        <source>Default Type</source>
        <translation>كۆڭۈلدىكى تۈرى</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="809"/>
        <source>Flatbed</source>
        <translation>تۈزلەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="819"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="828"/>
        <source>ADF Front</source>
        <translation>ADF ئالدى</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="837"/>
        <source>ADF Back</source>
        <translation>ADF كەينىگە</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="846"/>
        <source>ADF Duplex</source>
        <translation>ADF Duplex</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="896"/>
        <location filename="../src/saneobject.cpp" line="2195"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="899"/>
        <location filename="../src/saneobject.cpp" line="2199"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="902"/>
        <location filename="../src/saneobject.cpp" line="2203"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="905"/>
        <location filename="../src/saneobject.cpp" line="2207"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="908"/>
        <location filename="../src/saneobject.cpp" line="2211"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="911"/>
        <location filename="../src/saneobject.cpp" line="2215"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="914"/>
        <location filename="../src/saneobject.cpp" line="2219"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="917"/>
        <location filename="../src/saneobject.cpp" line="2223"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="920"/>
        <location filename="../src/saneobject.cpp" line="2227"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="938"/>
        <location filename="../src/saneobject.cpp" line="2243"/>
        <source>Auto</source>
        <translation>ئاپتۇماتىك</translation>
    </message>
    <message>
        <location filename="../src/device/ukui_apt.cpp" line="79"/>
        <location filename="../src/device/ukui_apt.cpp" line="160"/>
        <source>Install timeout.</source>
        <translation>ئورنىتىش ۋاقتى ئۆتتى.</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="1943"/>
        <source>Default Type</source>
        <translation>كۆڭۈلدىكى تۈرى</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1935"/>
        <source>Flatbed</source>
        <translation>تۈزلەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="416"/>
        <source>Refresh list complete.</source>
        <translation>يېڭىلاش تىزىملىكى مۇكەممەل.</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1937"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1939"/>
        <source>ADF Front</source>
        <translation>ADF ئالدى</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1941"/>
        <source>ADF Back</source>
        <translation>ADF كەينىگە</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1945"/>
        <source>ADF Duplex</source>
        <translation>ADF Duplex</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2021"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2023"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2025"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2027"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2029"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2031"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2033"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2035"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2037"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="61"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="65"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="85"/>
        <location filename="../src/scandialog.cpp" line="184"/>
        <source>Number of pages scanning: </source>
        <translation>بەت سىكاننېرلاش سانلىرى: </translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="97"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>سىكاننېرلاش ئەمەلدىن قالدۇرۇلدى، سەل ساقلاپ تۇرۇڭ!</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="182"/>
        <source>Multiple</source>
        <translation>كۆپ</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="754"/>
        <source>Scanner device</source>
        <translation>سىكاننېرلاش ئۈسكۈنىسى</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>File settings</source>
        <translation>ھۆججەت تەڭشەكلىرى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="586"/>
        <source>Device</source>
        <translation>ئۈسكۈنە</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="128"/>
        <source>Select a directory</source>
        <translation>مۇندەرىجە تاللاش</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="151"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>نۆۋەتتە ئىشلەتكۈچىنىڭ مۇندەرىجىنى ئۆزگەرتىش رۇخسىتى يوق </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="214"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>تەكشى كارىۋات سكاننېرلاش ئەندىزىسى كۆپ قېتىم سكاننېرلاشنى قوللىمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="219"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Multiple</source>
        <translation>كۆپ</translation>
    </message>
    <message>
        <source>This resolution will take a long time to scan, please choose carelly.</source>
        <translation type="vanished">بۇ پەرقلەندۈرۈش نىسبىتى ناھايىتى ئۇزۇن ۋاقىت كېتىدۇ، كۆڭۈل قويۇپ تاللاڭ.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="535"/>
        <source>Alert</source>
        <translation>ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="587"/>
        <source>Pages</source>
        <translation>بەت</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="588"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="282"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Color</source>
        <translation>رەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="590"/>
        <source>Resolution</source>
        <translation>پەرقلەندۈرۈش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="591"/>
        <source>Size</source>
        <translation>چوڭ-كىچىكلىكى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="592"/>
        <source>Format</source>
        <translation>فورماتى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="593"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="762"/>
        <source>scanner01</source>
        <translation>سكاننېر01</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="594"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="602"/>
        <location filename="../src/scansettingswidget.cpp" line="603"/>
        <source>Mail to</source>
        <translation>ئېلخەت ئارقىلىق</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="781"/>
        <location filename="../src/scansettingswidget.cpp" line="1142"/>
        <location filename="../src/scansettingswidget.cpp" line="1143"/>
        <source>Save as</source>
        <translation>تېجەش</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="216"/>
        <location filename="../src/scansettingswidget.cpp" line="240"/>
        <location filename="../src/scansettingswidget.cpp" line="271"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Single</source>
        <translation>يەككە تەنھا</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="296"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="297"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="298"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="vanished">بۇ قارارنى سىكاننېرلاش ئۈچۈن بىر قېتىم loog ۋاقتى كېتىدۇ، پەرۋىش قىلىشنى تاللاڭ.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="328"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>&apos;/&apos; خاراكتېرىنى ئۆز ئىچىگە ئالالمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="333"/>
        <location filename="../src/scansettingswidget.cpp" line="470"/>
        <source>cannot save as hidden file.</source>
        <translation>يوشۇرۇن ھۆججەت سۈپىتىدە ساقلىغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="459"/>
        <source>Save As</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="483"/>
        <source>Path without access rights: </source>
        <translation>زىيارەت ھوقۇقى يوق يول: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="487"/>
        <source>File path that does not exist: </source>
        <translation>مەۋجۇت بولمىغان ھۆججەت يولى: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="533"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation>ئاللىقاچان مەۋجۇت، كۆچۈرۈپ يېزىپ قويماقمۇ؟ ئەگەر سىز كۆپ بەتنى سكاننېرلىغان بولسىڭىز، ئۇ كۆپ ھۆججەتنى ئورتاق يېزىشنى كەلتۈرۈپ چىقىرىشى مۇمكىن. ئېھتىياتچان بولۇڭ !</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="731"/>
        <source>Start Scan</source>
        <translation>سكاننېرلاش باشلاندى</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1133"/>
        <location filename="../src/scansettingswidget.cpp" line="1134"/>
        <source>Store text</source>
        <translation>تېكىستنى ساقلاش</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="532"/>
        <source>The file </source>
        <translation>ھۆججەت </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="vanished">已存在，您想覆盖它吗？</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="589"/>
        <source>Colour</source>
        <translation>رەڭ</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="270"/>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>Flatbed</source>
        <translation>تۈزلەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Gray</source>
        <translation>كۈلرەڭ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="284"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Lineart</source>
        <translation>Lineart</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="300"/>
        <source>This resolution will take a long time to scan, please choose carefully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1047"/>
        <source>Resolution is empty!</source>
        <translation>ھەل قىلىش قۇۋۋىتى قۇرۇق!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1362"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="194"/>
        <location filename="../src/sendmail.cpp" line="218"/>
        <source>Select email client</source>
        <translation>ئېلخەت خېرىدارىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="200"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="203"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="232"/>
        <location filename="../src/sendmail.cpp" line="233"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="236"/>
        <location filename="../src/sendmail.cpp" line="237"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="64"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="72"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1113"/>
        <source>Canceling...Please waiting...</source>
        <translation>ئەمەلدىن قالدۇرۇلدى... ساقلاڭ...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1124"/>
        <source>Running beauty ...</source>
        <translation>يۈگرەيدىغان ساھىبجامال ...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1157"/>
        <source>Running rectify ...</source>
        <translation>يۈگۈرۈشنى تەرتىپكە سېلىش ...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>kylin-scanner</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="194"/>
        <location filename="../src/titlebar/titlebar.h" line="82"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>Option</source>
        <translation>تاللانما</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="179"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="220"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="261"/>
        <location filename="../src/titlebar/titlebar.cpp" line="145"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="308"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="53"/>
        <source>Refresh List</source>
        <translation>يېڭىلاش تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="70"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="74"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="76"/>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="77"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ئۇچۇر LAN دا قىسقا پاراڭلىشىش ۋە ھۆججەت يۆتكەش ئىقتىدارى بىلەن تەمىنلەيدۇ. مۇلازىمىتېر قۇرۇشنىڭ ھاجىتى يوق. ئۇ كۆپ ئادەمنىڭ بىرلا ۋاقىتتا ھەمكارلىشىۋېلىشىنى ۋە پاراللېل يوللاش ۋە قوبۇل قىلىشنى قوللايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="87"/>
        <source>Exit</source>
        <translation>چىقىش ئېغىزى</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="142"/>
        <source>Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="192"/>
        <source>The current file is not saved. Do you want to save it?</source>
        <translation>ھازىرقى ھۆججەت ساقلانمىدى. قۇتقۇزۇپ قالامدۇ؟</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="vanished">当前文档未保存，是否保存？</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="196"/>
        <source>Straight &amp;Exit</source>
        <translation>تۈز&amp;چىقىش</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="197"/>
        <source>&amp;Save Exit</source>
        <translation>چىقىش ئېغىزىنى ساقلاش</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="74"/>
        <location filename="../src/toolbarwidget.cpp" line="168"/>
        <source>Beauty</source>
        <translation>ھۆسىن تۈزەش</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="80"/>
        <location filename="../src/toolbarwidget.cpp" line="174"/>
        <source>Rectify</source>
        <translation>تۈزىتىپ تۈزەش</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="86"/>
        <location filename="../src/toolbarwidget.cpp" line="180"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="94"/>
        <location filename="../src/toolbarwidget.cpp" line="188"/>
        <source>Crop</source>
        <translation>زىرائەتلەر</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="100"/>
        <location filename="../src/toolbarwidget.cpp" line="194"/>
        <source>Rotate</source>
        <translation>نۆۋەتتە</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="106"/>
        <location filename="../src/toolbarwidget.cpp" line="200"/>
        <source>Mirror</source>
        <translation>ئەينەك تەسۋىرى</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="112"/>
        <location filename="../src/toolbarwidget.cpp" line="206"/>
        <source>Watermark</source>
        <translation>سۇ بەلگە</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="131"/>
        <location filename="../src/toolbarwidget.cpp" line="225"/>
        <source>ZoomIn</source>
        <translation>ZoomIn</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="125"/>
        <location filename="../src/toolbarwidget.cpp" line="219"/>
        <source>ZoomOut</source>
        <translation>ZoomOut</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation type="vanished">扫描仪已断开连接！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="95"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>سۈرۈشتۈرۈش سكاننېر ئۈسكۈنىسى. كۈتۈپ تۇرۇپ بېقىڭ...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="119"/>
        <source>New Scanner has been Connected.</source>
        <translation>يېڭى سكاننېر ئۇلاندى.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="129"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>بىر سكاننېر ئۇلاش ئۈزۈلۈپ قالدى. ئەگەر سكاننېرنىڭ ئۇلىنىشىنى ئۈزۈپ سكاننېرلىغان بولسا، سكاننېرلىنى ئەمەلدىن قالدۇرۇش ياكى سكاننېرنىڭ خاتالىق ئۇچۇرىنى دوكلات قىلىشىنى ساقلاڭ.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="135"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>سكاننېر ئۇلىنىشنى ئۈزۈپ، سكاننېر تىزىملىكىنى يېڭىلىدى. كۈتۈپ تۇرۇپ بېقىڭ...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="150"/>
        <source>Scanner list refresh complete.</source>
        <translation>سكاننېر تىزىملىكى يېڭىلاش مۇكەممەل.</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="38"/>
        <source>Scanner</source>
        <translation>سىننەر</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="50"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="62"/>
        <source>Searching for scanner...</source>
        <translation>سكاننېر ئىزدەش...</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="47"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="66"/>
        <source>Add watermark</source>
        <translation>سۇ بەلگىسى قوشۇش</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="85"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="185"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>ھۆججەتنى &lt;filename&gt;ئېچىش</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="186"/>
        <source>Filename</source>
        <translation>ھۆججەت نامى</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="190"/>
        <source>Hide scan settings widget</source>
        <translation>سىكاننېرلاش تەڭشەكلىرىنى ۋىكىپېدىيەنى يوشۇرۇش</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Symbol</source>
        <translation>بەلگە</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>Alert</source>
        <translation>ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <source>You need to manually confirm whether the printer devices included in the list are scanning and printing all-in-one machines. If they are not scanning and printing all-in-one machines, ignore them.</source>
        <translation>تىزىملىككە كىرگۈزۈلگەن پرىنتېر ئۈسكۈنىسىنىڭ بىر گەۋدىلەشكەن ماشىنىنى سىكانىرلاپ بېسىۋاتقان-بېسىپ چىقىرىۋاتقان-بېسىپ چىقىرىۋاتقان-چىقىرىش ياكى ئەمەسلىكىنى قول بىلەن جەزملەشتۈرۈشىڭىز كېرەك. ئەگەر ئۇلار بىر گەۋدىلەشكەن ماشىنىنى سىكانىرلىمىسا، سەل قاراڭ.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="71"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="84"/>
        <source>Device List</source>
        <translation>ئۈسكۈنە تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="97"/>
        <location filename="../src/newdevicelistpage.cpp" line="117"/>
        <location filename="../src/newdevicelistpage.cpp" line="318"/>
        <location filename="../src/newdevicelistpage.cpp" line="375"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="98"/>
        <source>Next</source>
        <translation>كېيىنكى بەت</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="118"/>
        <source>Before</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="119"/>
        <source>Install</source>
        <translation>قاچىلاش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="197"/>
        <source>Scanner</source>
        <translation>سكاننېر</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="199"/>
        <source>Printer</source>
        <translation>پرىنتېر</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="216"/>
        <source>Device Name:</source>
        <translation>ئۈسكۈنە نامى :</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="220"/>
        <source>Driver Name:</source>
        <translation>شوپۇرنىڭ نامى :</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <source>This driver is from third party, may cause some unmetable result.</source>
        <translation>بۇ شوپۇر ئۈچىنچى تەرەپتىن كەلگەن بولۇپ، بەزى ئۆلچەشكە بولمايدىغان نەتىجە پەيدا قىلىشى مۇمكىن.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>This driver is provided by the manufacturer. Please contact the corresponding manufacturer of the device to obtain the driver program.</source>
        <translation>بۇ شوپۇرنى زاۋۇت تەمىنلەيدۇ. ئۈسكۈنىنىڭ مۇناسىپ زاۋۇتى بىلەن ئالاقىلىشىپ شوپۇرلۇق پروگراممىسىغا ئېرىشىڭ.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="312"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>قوللىنىشقا بولىدىغان شوپۇرلار يوق، قول بىلەن شوپۇرنى قوشۇپ قويماقمۇ؟</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="316"/>
        <source>Add</source>
        <translation>قوشۇش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="353"/>
        <location filename="../src/newdevicelistpage.cpp" line="458"/>
        <source>Installing driver...</source>
        <translation>قوراشتۇرۇش شوپۇر...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="370"/>
        <source>Installation successful. Do you want to use this scanner now? Click &apos;Use&apos; to reset udev (administrator password required) and restart the scanning application to refresh the device list. If you still want to continue operating the scanned image, you will need to manually restart and use the scanning function.</source>
        <translation>قۇراشتۇرۇش مۇۋەپپەقىيەتلىك بولدى. ھازىر بۇ سكاننېرنى ئىشلىتىشكە بارمۇ؟ «ئىشلىتىش» نى چېكىپ Udev ( باشقۇرغۇچى مەخپىي نومۇرى زۆرۈر ) نى قايتا تەسىس قىلىپ، سكاننېرلاش قوللىنىشىنى قايتا قوزغىتىپ ئۈسكۈنە تىزىملىكىنى يېڭىلاڭ. ئەگەر يەنىلا سىكاننېرلاپ ئىشلىتىلىدىغان سۈرەتنى داۋاملىق مەشغۇلات قىلماقچى بولسىڭىز، قول بىلەن قايتا قوزغىتىلىشى ھەمدە سىكاننېرلاش ئىقتىدارىنى ئىشلىتىشىڭىز كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="376"/>
        <source>Use</source>
        <translation>ئىشلىتىش</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="407"/>
        <source>Installation failed.</source>
        <translation>قۇراشتۇرۇش مەغلۇپ بولدى.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="410"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="452"/>
        <source>Select a directory</source>
        <translation>مۇندەرىجە تاللاش</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="48"/>
        <location filename="../src/showocrwidget.cpp" line="157"/>
        <location filename="../src/showocrwidget.cpp" line="165"/>
        <source>The document is in character recognition ...</source>
        <translation>ھۆججەت خاراكتېرىنى پەرقلەندۈرۈشتە ...</translation>
    </message>
</context>
</TS>
