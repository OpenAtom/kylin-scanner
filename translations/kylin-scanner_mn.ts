<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>DetectPageWidget</name>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="48"/>
        <source>Detect scanners, please waiting</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ ᠳᠤᠮᠳᠠ ᠂ ᠲᠦᠷ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠁ . . .  </translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠰᠢᠷᠪᠢᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠣᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠂ ᠤᠷᠢᠳ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠰᠢᠷᠪᠢᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠲᠡᠢ ᠵᠠᠯᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ  </translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="65"/>
        <source>No available scan devices</source>
        <translation>ᠨᠣᠸᠠᠸᠠ ᠶᠢᠨ ᠢᠷᠠᠪᠰᠺᠣᠳ᠋ᠸᠢᠰ ᠃</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="69"/>
        <source>Connect</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠰᠢᠷᠪᠢᠭᠦᠷ  </translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="46"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="58"/>
        <source>Unable to read text, please retrey</source>
        <translation>ᠲ᠋ᠧᠺᠰᠲ ᠤᠨᠢᠬᠤ ᠵᠠᠯᠠᠭᠤᠷᠢᠭᠤᠯᠤᠭᠠ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠢᠳᠠᠨ ᠠᠭᠤᠯᠠᠭᠠᠷᠠᠢ ᠨᠠᠢᠢᠷᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠂</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>ᠲᠤᠬᠠᠢ  </translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="73"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠰᠢᠷᠪᠢᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠲᠡᠢ ᠬᠣᠯᠪᠣᠪᠠ ᠂ ᠲᠣᠪᠴᠢ᠎ᠶ᠋ᠢᠨ ᠡᠬᠢᠯᠡᠨ ᠰᠢᠷᠪᠢᠬᠦ  
</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new scanner to refresh the device list.</source>
        <translation>ᠨᠣᠰᠺᠠᠨᠠᠳ᠋ᠲ᠋ᠧᠺᠲ᠋ᠧᠳ᠋ ᠂ ᠫᠯᠦᠭ ᠨᠢᠦ᠋ᠰᠺᠠᠨᠠᠲ᠋ᠧᠷᠨᠠᠲ᠋ᠧᠷ ᠸᠢᠱᠲ᠋ᠧᠷᠧᠳ᠋ ᠸᠢᠰᠲ᠋ᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>No scanner detected, plug in a new USB scanner or click refresh list to refresh the device list.</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠢ᠋ ᠰᠢᠯᠭᠠᠨ ᠣᠯᠤᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠂ ᠰᠢᠨ᠎ᠡ usb ᠰᠢᠷᠪᠢᠭᠦᠷ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠨᠡᠳᠬᠡᠭᠰᠡᠨ ᠬᠦᠰᠦᠨᠦᠭ᠎ᠢ᠋ ᠲᠤᠶᠠᠵᠤ ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠦᠰᠦᠨᠦᠭ᠎ᠢ᠋ ᠰᠢᠨᠡᠳᠬᠡᠵᠦ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃  </translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Scanner</source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠯᠲᠡ ᠂ ᠰᠢᠷᠪᠢᠯᠲᠡ  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="171"/>
        <source>Ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="434"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠂ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ᠎ᠢ᠋ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠲᠦ᠍ ᠰᠣᠯᠢ ᠃  </translation>
    </message>
    <message>
        <source>error code:</source>
        <translation type="vanished">ᠲᠡᠰᠢᠶ᠎ᠡ ᠺᠣᠳ᠋ ᠄  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="466"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠠᠭᠠᠷᠠᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠪᠤᠰᠤᠳ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠢ᠋ ᠬᠦᠯᠢᠭᠡᠷᠡᠢ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠪᠤᠶᠤ ᠰᠣᠯᠢᠵᠤ ᠠᠪᠤᠭᠠᠷᠠᠢ ᠃  
</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="470"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠦᠷ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠂ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠢ᠋ ᠳᠠᠬᠢᠨ ᠰᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠰᠢᠷᠪᠢᠨ᠎ᠡ ᠃  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="474"/>
        <source>Scan operation has been cancelled.</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠭᠳᠠᠪᠠ !︕  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="478"/>
        <source>Scan failed, operation is not supported.</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠠᠴᠠ ᠪᠣᠯᠵᠤ ᠂ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="481"/>
        <source>Scan failed, Document fedder jammed.</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠨ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠳᠡᠰ ᠢ ᠴᠠᠭᠠᠰᠤᠨ ᠪᠠᠭᠠᠵᠢ ᠣᠷᠣᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="484"/>
        <source>Scan failed, Error during device I/O.</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ i/O ᠶᠠᠪᠤᠴᠠ ᠳᠤ ᠪᠤᠷᠤᠭᠤ ᠭᠠᠷᠴᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="487"/>
        <source>Scan failed, Out of memory.</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠳᠣᠲᠣᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠳᠤᠲᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="490"/>
        <source>Scan failed, Access to resource has been denied.</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠤᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠭᠠ ᠶᠢ ᠲᠡᠪᠴᠢᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="493"/>
        <source>Scan failed, Scanner cover is open.</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦ ᠢᠯᠠᠭᠳᠠᠯ ᠂ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠦᠨ ᠪᠦᠷᠬᠦᠭᠦᠯ ᠨᠡᠭᠡᠭᠡᠭᠳᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="497"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠣᠳᠣᠬᠢ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠢ᠋ ᠵᠠᠯᠭᠠᠬᠤ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠲᠦ᠍ ᠰᠣᠯᠢᠬᠤ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ ᠃  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="544"/>
        <source>System sleep/sleep detected. To ensure normal use of the scanner, please click on restart to restart the scanner application. If you want to continue operating the scanned image, click cancel, but the scanning related functions will be disabled and will take effect after restarting.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠤᠨᠲᠠᠬᠤ/ᠤᠨᠲᠠᠬᠤ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠰᠢᠯᠭᠠᠪᠠ ᠃ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠦᠨ ᠬᠡᠪ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠶᠢ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠶᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠂ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠶᠢ ᠳᠠᠷᠤᠵᠤ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠪᠣᠯᠪᠠ ᠃ ᠬᠡᠷᠪᠡ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠭᠡᠪᠡᠯ ᠂ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ ᠶᠢ ᠳᠠᠷᠤᠨᠠ ᠂ ᠭᠡᠪᠡᠴᠦ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠬᠠᠮᠢᠶᠠ ᠪᠦᠬᠦᠢ ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ ᠪᠥᠭᠡᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭᠠ ᠬᠦᠴᠦᠨ ᠲᠡᠶ ᠪᠣᠯᠤᠨᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="549"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="550"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>Alert</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="579"/>
        <source>A new Scanner has been connected.</source>
        <translation>ᠨᠢᠭᠡ ᠰᠢᠨ᠎ᠡ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠵᠠᠯᠭᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠵᠠᠢ ᠃  </translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="620"/>
        <source>Running beauty ...</source>
        <translation>ᠨᠢᠭᠡ ᠳᠠᠷᠤᠭᠤᠯ᠎ᠢ᠋ ᠭᠤᠶᠤᠵᠢᠭᠤᠯᠬᠤ ᠳᠤᠮᠳᠠ . .  
</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="641"/>
        <source>Running rectify ...</source>
        <translation>ᠣᠶᠤᠨᠲᠤ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠳᠤᠮᠳᠠ . .  </translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="43"/>
        <source>Scanner not detected</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠨ ᠣᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠰᠢᠷᠪᠢᠭᠦᠷ</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="58"/>
        <location filename="../src/sendmail.cpp" line="81"/>
        <location filename="../src/sendmail.cpp" line="82"/>
        <source>No email client</source>
        <translation>ᠰᠢᠤᠳᠠᠨ ᠵᠦᠢᠯ ᠬᠡᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="65"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ  </translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="93"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>ᠣᠳᠣᠬᠢ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠳ᠋ᠦ᠍ ᠰᠢᠤᠳᠠᠨ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠣᠯᠪᠠ ᠂ ᠤᠷᠢᠳᠠᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠰᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃  </translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="105"/>
        <source>Cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ  </translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="107"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ᠎ᠪᠠᠷ ᠶᠠᠪᠤᠬᠤ  </translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="161"/>
        <location filename="../src/scandialog.cpp" line="46"/>
        <location filename="../src/scansettingswidget.cpp" line="1359"/>
        <location filename="../src/mainwidget.cpp" line="70"/>
        <location filename="../src/mainwidget.cpp" line="169"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠪᠢᠯᠲᠡ ᠂ ᠰᠢᠷᠪᠢᠯᠲᠡ  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="212"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>Multiple</source>
        <translation>ᠣᠯᠠᠨ ᠨᠢᠭᠤᠷ᠎ᠲᠠᠢ ᠰᠢᠷᠪᠢᠯᠲᠡ  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="581"/>
        <source>Fail to open the scanner</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠦᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <location filename="../src/saneobject.cpp" line="327"/>
        <location filename="../src/saneobject.cpp" line="444"/>
        <location filename="../src/saneobject.cpp" line="506"/>
        <location filename="../src/scansettingswidget.cpp" line="516"/>
        <location filename="../src/scansettingswidget.cpp" line="538"/>
        <source>ADF Duplex</source>
        <translation>ADF ᠬᠣᠶᠠᠷ ᠲᠠᠯᠠᠲᠤ  </translation>
    </message>
    <message>
        <source>Fail to open the scanner, error code </source>
        <translation type="vanished">ᠰᠢᠷᠪᠢᠭᠦᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ᠎ᠤ᠋ᠨ ᠺᠣᠳ᠋  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2017"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ  </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="213"/>
        <source>Flatbed</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠬᠠᠪᠲᠠᠰᠤ  </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="59"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>ᠶᠠᠭ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ᠎ᠶ᠋ᠢ ᠰᠢᠨᠡᠳᠬᠡᠵᠦ ᠪᠣᠢ ᠂ ᠠᠮᠵᠢᠯᠲᠠ᠎ᠲᠠᠢ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ᠎ᠶ᠋ᠢ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠪᠣᠢ ᠁  </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Scanner is on detecting...</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠦᠷ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠰᠢᠯᠭᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠳᠤᠮᠳᠠ . . .  </translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="257"/>
        <source>Single</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="652"/>
        <source>Color</source>
        <translation>ᠥᠩᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="659"/>
        <source>Gray</source>
        <translation>ᠪᠣᠷᠣ ᠥᠩᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="665"/>
        <source>Lineart</source>
        <translation>ᠵᠢᠷᠤᠭᠠᠰᠤᠨ ᠤᠷᠠᠯᠢᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="730"/>
        <source>Default Type</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠬᠤ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="809"/>
        <source>Flatbed</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="819"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="828"/>
        <source>ADF Front</source>
        <translation>ADF ᠡᠮᠦᠨ᠎ᠡ ᠲᠠᠯ᠎ᠠ  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="837"/>
        <source>ADF Back</source>
        <translation>ADF ᠠᠷᠤ ᠨᠢᠭᠤᠷ  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="846"/>
        <source>ADF Duplex</source>
        <translation>ADF ᠬᠣᠶᠠᠷ ᠲᠠᠯᠠᠲᠤ  </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="896"/>
        <location filename="../src/saneobject.cpp" line="2195"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="899"/>
        <location filename="../src/saneobject.cpp" line="2199"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="902"/>
        <location filename="../src/saneobject.cpp" line="2203"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="905"/>
        <location filename="../src/saneobject.cpp" line="2207"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="908"/>
        <location filename="../src/saneobject.cpp" line="2211"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="911"/>
        <location filename="../src/saneobject.cpp" line="2215"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="914"/>
        <location filename="../src/saneobject.cpp" line="2219"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="917"/>
        <location filename="../src/saneobject.cpp" line="2223"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="920"/>
        <location filename="../src/saneobject.cpp" line="2227"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="938"/>
        <location filename="../src/saneobject.cpp" line="2243"/>
        <source>Auto</source>
        <translation>ᠮᠠᠰᠢᠨ ᠲᠡᠷᠭᠡ᠃</translation>
    </message>
    <message>
        <source>Current </source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠃ </translation>
    </message>
    <message>
        <source> User</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <source> has already opened kylin-scanner, open will close </source>
        <translation type="vanished"> ᠾᠧ ᠯᠢᠨ ᠢ ᠰᠢᠷᠦᠭᠦᠨ ᠬᠠᠭᠠᠬᠤ ᠪᠠᠷ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃ </translation>
    </message>
    <message>
        <source>&apos;s operations. Are you continue?</source>
        <translation type="vanished">ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠃ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠬᠦ ᠦᠦ ? ︖  </translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">ᠠᠰᠠᠭᠤᠳᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../src/device/ukui_apt.cpp" line="79"/>
        <location filename="../src/device/ukui_apt.cpp" line="160"/>
        <source>Install timeout.</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠲᠦᠷᠡᠪᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="416"/>
        <source>Refresh list complete.</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1935"/>
        <source>Flatbed</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1937"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1939"/>
        <source>ADF Front</source>
        <translation>ADF ᠡᠮᠦᠨ᠎ᠡ ᠹᠷᠣᠨᠲ᠋ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1941"/>
        <source>ADF Back</source>
        <translation>ADF ᠪᠤᠴᠠᠵᠤ ᠢᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1943"/>
        <source>Default Type</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠬᠤ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1945"/>
        <source>ADF Duplex</source>
        <translation>ADF ᠬᠣᠣᠰ ᠠᠵᠢᠯᠴᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2021"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2023"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2025"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2027"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2029"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2031"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2033"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2035"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2037"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="61"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="65"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="85"/>
        <location filename="../src/scandialog.cpp" line="184"/>
        <source>Number of pages scanning: </source>
        <translation>ᠨᠢᠭᠤᠷ ᠤᠨ ᠲᠣᠭ᠎ᠠ ᠶᠢ ᠰᠢᠷᠭᠦᠨ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="97"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>ᠶᠠᠭ ᠰᠢᠷᠦᠭᠦᠨ ᠱᠦᠭᠦᠷᠳᠡᠬᠦ ᠪᠡᠨ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="182"/>
        <source>Multiple</source>
        <translation>ᠳᠠᠬᠢᠨ ᠲᠣᠭ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="128"/>
        <source>Select a directory</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="151"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠭᠠᠷᠴᠠᠭ ᠲᠤ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠷᠬᠡ ᠦᠭᠡᠶ᠃ </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="214"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠶ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠨᠢ ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="216"/>
        <location filename="../src/scansettingswidget.cpp" line="240"/>
        <location filename="../src/scansettingswidget.cpp" line="271"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Single</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="219"/>
        <location filename="../src/scansettingswidget.cpp" line="961"/>
        <source>Multiple</source>
        <translation>ᠳᠠᠬᠢᠨ ᠲᠣᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="270"/>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>Flatbed</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="282"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Color</source>
        <translation>ᠥᠩᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="284"/>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Lineart</source>
        <translation>ᠵᠢᠷᠤᠭᠠᠰᠤᠨ ᠤᠷᠠᠯᠢᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="296"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="297"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="298"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="vanished">ᠡᠨᠡ ᠢᠯᠭᠠᠴᠠ ᠨᠢ ᠲᠤᠩ ᠤᠷᠲᠤ ᠴᠠᠭ ᠬᠡᠷᠡᠭᠰᠡᠪᠡᠯ ᠰᠠᠶ ᠢ ᠰᠢᠷᠪᠢᠵᠦ ᠳᠡᠶᠢᠯᠦᠨ᠎ᠡ ᠂ ᠪᠣᠯᠭᠣᠮᠵᠢᠲᠠᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>This resolution will take a long time to scan, please choose carelly.</source>
        <translation type="vanished">ᠡᠨᠡ ᠬᠦ ᠢᠯᠭᠠᠮᠵᠢ ᠶᠢ ᠮᠠᠰᠢ ᠤᠷᠲᠤ ᠬᠤᠭᠤᠴᠠᠭᠠ ᠪᠠᠷ ᠰᠢᠷᠪᠢᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠂ ᠪᠣᠯᠭᠣᠮᠵᠢᠲᠠᠶ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="535"/>
        <source>Alert</source>
        <translation type="unfinished">ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ  </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="328"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>《 / 》 ᠭᠡᠰᠡᠨ ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="300"/>
        <source>This resolution will take a long time to scan, please choose carefully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="333"/>
        <location filename="../src/scansettingswidget.cpp" line="470"/>
        <source>cannot save as hidden file.</source>
        <translation>ᠠᠰᠲᠠᠭᠠᠨ ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢᠶᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="459"/>
        <source>Save As</source>
        <translation>ᠠᠰᠲᠠᠭᠠᠨ ᠬᠠᠳᠠᠭᠠᠯᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="483"/>
        <source>Path without access rights: </source>
        <translation>ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠭᠡᠢ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="487"/>
        <source>File path that does not exist: </source>
        <translation>ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠄ </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="532"/>
        <source>The file </source>
        <translation>ᠲᠤᠰ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠃ </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="533"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation> ᠨᠢᠭᠡᠨᠲᠡ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠂ ᠲᠡᠭᠦᠨ ᠢ ᠪᠥᠷᠬᠥᠭᠦᠯᠬᠦ ᠦᠦ ? ᠬᠡᠷᠪᠡ ᠣᠯᠠᠨ ᠨᠢᠭᠤᠷ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠪᠡᠯ ᠂ ᠣᠯᠠᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠮᠤᠷᠬᠤ ᠪᠣᠯᠣᠮᠵᠢ ᠲᠠᠶ ᠃ ᠪᠣᠯᠭᠤᠮᠵᠢᠲᠠᠢ ᠪᠠᠶᠢᠭᠠᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠮᠡᠷᠭᠡᠵᠢᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="586"/>
        <source>Device</source>
        <translation>ᠲᠣᠨᠣᠭ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="587"/>
        <source>Pages</source>
        <translation>ᠨᠢᠭᠤᠷ ᠤᠨ ᠭᠠᠳᠠᠷᠭᠤ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="588"/>
        <source>Type</source>
        <translation>ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="589"/>
        <source>Colour</source>
        <translation>ᠥᠩᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="590"/>
        <source>Resolution</source>
        <translation>ᠢᠯᠭᠠᠴᠠ ᠶᠢᠨ ᠨᠣᠷᠮ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="591"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="592"/>
        <source>Format</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="593"/>
        <source>Name</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="594"/>
        <source>Save</source>
        <translation>ᠠᠪᠤᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="602"/>
        <location filename="../src/scansettingswidget.cpp" line="603"/>
        <source>Mail to</source>
        <translation>ᠰᠢᠤᠳᠠᠨ ᠢᠶᠠᠷ ᠶᠠᠪᠤᠭᠤᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="731"/>
        <source>Start Scan</source>
        <translation>ᠡᠬᠢᠯᠡᠨ ᠰᠢᠷᠪᠡᠭᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="754"/>
        <source>Scanner device</source>
        <translation>ᠬᠡᠮᠵᠢᠭᠦᠷᠯᠢᠭ᠍ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠰᠢᠷᠪᠡᠭ᠍ᠳᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>File settings</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠪᠠᠢ᠌ᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="762"/>
        <source>scanner01</source>
        <translation>01 ᠶᠢ ᠰᠢᠷᠪᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="781"/>
        <location filename="../src/scansettingswidget.cpp" line="1142"/>
        <location filename="../src/scansettingswidget.cpp" line="1143"/>
        <source>Save as</source>
        <translation>ᠠᠰᠲᠠᠭᠠᠨ ᠬᠠᠳᠠᠭᠠᠯᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="978"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1007"/>
        <source>Gray</source>
        <translation>ᠪᠣᠷᠣ ᠥᠩᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1030"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1047"/>
        <source>Resolution is empty!</source>
        <translation>ᠢᠯᠭᠠᠴᠠᠯ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ !</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1062"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1133"/>
        <location filename="../src/scansettingswidget.cpp" line="1134"/>
        <source>Store text</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠢᠴᠢᠭ᠌</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1362"/>
        <source>Yes</source>
        <translation>ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="194"/>
        <location filename="../src/sendmail.cpp" line="218"/>
        <source>Select email client</source>
        <translation>ᠡᠯᠧᠺᠲ᠋ᠷᠣᠨ ᠰᠢᠦᠳᠠᠨ ᠤ ᠵᠦᠢᠯ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="200"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="203"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="232"/>
        <location filename="../src/sendmail.cpp" line="233"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="236"/>
        <location filename="../src/sendmail.cpp" line="237"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="64"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="72"/>
        <source>Ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1113"/>
        <source>Canceling...Please waiting...</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1124"/>
        <source>Running beauty ...</source>
        <translation>ᠭᠦᠶᠦᠯᠲᠡ ᠶᠢᠨ ᠭᠤᠶᠤ ᠬᠡᠦᠬᠡᠨ ᠤ ᠁</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1157"/>
        <source>Running rectify ...</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠢ ᠪᠦᠬᠦᠯᠢ ᠪᠠᠷ ᠨᠢ ᠤᠷᠤᠰᠬᠠᠬᠤ ᠁</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="194"/>
        <location filename="../src/titlebar/titlebar.h" line="82"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>Option</source>
        <translation>ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="179"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="220"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="261"/>
        <location filename="../src/titlebar/titlebar.cpp" line="145"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠶᠡᠬᠡ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="308"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="53"/>
        <source>Refresh List</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="70"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="74"/>
        <source>About</source>
        <translation>ᠪᠠᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="76"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="77"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠦ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠶᠠᠷᠢᠯᠴᠠᠬᠤ ᠪᠠ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠴᠢᠳᠠᠪᠬᠢ ᠬᠠᠩᠭᠠᠵᠠᠢ ᠃ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠ ᠦᠭᠡᠢ ᠃ ᠲᠡᠷᠡ ᠣᠯᠠᠨ ᠬᠥᠮᠥᠨ ᠬᠠᠮᠲᠤᠪᠠᠷ ᠬᠠᠷᠢᠯᠴᠠᠨ ᠬᠠᠷᠢᠯᠴᠠᠨ ᠨᠡᠢ᠌ᠯᠡᠭᠦᠯᠬᠦ ᠶᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="87"/>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="142"/>
        <source>Restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="192"/>
        <source>The current file is not saved. Do you want to save it?</source>
        <translation>ᠲ᠋ᠧᠯᠦᠢᠨᠲ᠋ᠹᠧᠷᠹᠢᠯ ᠢᠰᠨᠤᠲ᠋ᠰᠠᠸᠧᠳ᠋ ᠃ ᠸᠠᠨᠲ᠋ᠧ ᠲᠥᠸᠧᠢᠲ᠋ ᠬᠡᠳᠦᠢ ᠪᠠᠢ᠌ᠬᠤ ᠪᠤᠢ ?</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠳᠠᠩᠰᠠ ᠡᠪᠬᠡᠮᠡᠯ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠶ ᠃ ᠲᠡᠭᠦᠨ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="196"/>
        <source>Straight &amp;Exit</source>
        <translation>ᠰᠢᠭᠤᠳ ᠶᠠᠪᠤᠬᠤ ᠪᠠ ᠪᠤᠴᠠᠭᠠᠨ ᠭᠠᠷᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="197"/>
        <source>&amp;Save Exit</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠠᠶᠢᠭᠠᠳ ᠪᠤᠴᠠᠭᠠᠨ ᠭᠠᠷᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="74"/>
        <location filename="../src/toolbarwidget.cpp" line="168"/>
        <source>Beauty</source>
        <translation>ᠭᠣᠶᠣ ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="80"/>
        <location filename="../src/toolbarwidget.cpp" line="174"/>
        <source>Rectify</source>
        <translation>ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="86"/>
        <location filename="../src/toolbarwidget.cpp" line="180"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="94"/>
        <location filename="../src/toolbarwidget.cpp" line="188"/>
        <source>Crop</source>
        <translation>ᠲᠠᠷᠢᠮᠠᠯ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="100"/>
        <location filename="../src/toolbarwidget.cpp" line="194"/>
        <source>Rotate</source>
        <translation>ᠡᠷᠭᠢᠯᠳᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="106"/>
        <location filename="../src/toolbarwidget.cpp" line="200"/>
        <source>Mirror</source>
        <translation>ᠲᠣᠯᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="112"/>
        <location filename="../src/toolbarwidget.cpp" line="206"/>
        <source>Watermark</source>
        <translation>ᠤᠰᠤᠨ ᠳᠠᠷᠤᠮᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="125"/>
        <location filename="../src/toolbarwidget.cpp" line="219"/>
        <source>ZoomOut</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="131"/>
        <location filename="../src/toolbarwidget.cpp" line="225"/>
        <source>ZoomIn</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="95"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>ᠶᠠᠭ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠰᠢᠷᠦᠭᠦᠷᠳᠡᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="119"/>
        <source>New Scanner has been Connected.</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠰᠢᠷᠠᠭᠰᠠᠨ ᠢᠰᠬᠦᠯ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="129"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>ᠰᠢᠷᠠᠬᠤ ᠢᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠲᠠᠰᠤᠯᠵᠤ ᠵᠠᠯᠭᠠᠪᠠ᠃ ᠬᠡᠷᠪᠡ ᠡᠷᠡᠰ ᠰᠢᠷᠬᠠᠲᠤᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠭᠦᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠶᠠᠭ ᠰᠢᠷᠦᠭᠦᠷᠯᠡᠵᠦ ᠪᠠᠶᠢᠭ᠎ᠠ ᠬᠣᠯᠪᠣᠭ᠎ᠠ ᠶᠢ ᠰᠢᠷᠦᠭᠦᠨ ᠠᠪᠴᠤ ᠪᠠᠢ᠌ᠪᠠᠯ ᠂ ᠰᠢᠷᠦᠭᠦᠷ ᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠷᠦᠭᠦᠷ ᠦᠨ ᠮᠡᠳᠡᠭᠦᠯᠦᠯᠲᠡ ᠶᠢᠨ ᠪᠤᠷᠤᠭᠤ ᠴᠢᠮᠡᠭᠡ ᠶᠢ ᠭᠠᠭᠴᠠᠭᠠᠷᠳᠠᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠪᠠᠢ᠌ᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="135"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>ᠰᠢᠷᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠲᠠᠰᠤᠷᠠᠯᠲᠠ ᠦᠭᠡᠢ ᠬᠣᠯᠪᠣᠵᠤ ᠂ ᠰᠢᠷᠪᠡᠯᠲᠡ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯ ᠤᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠵᠡᠢ ᠃ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="150"/>
        <source>Scanner list refresh complete.</source>
        <translation>ᠰᠢᠷᠠᠬᠤ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠥᠰᠥᠨᠥᠭᠲᠦ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠪᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="38"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠦᠷ; ᠰᠢᠷᠪᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="50"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="62"/>
        <source>Searching for scanner...</source>
        <translation>ᠶᠠᠭ ᠡᠷᠢᠵᠦ ᠪᠠᠢᠢᠭᠠᠳ ᠱᠦᠭᠦᠷᠳᠡᠬᠦ ᠱᠦᠭᠦᠷᠳᠡᠬᠦ ᠁</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="47"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="66"/>
        <source>Add watermark</source>
        <translation>ᠤᠰᠤ ᠨᠡᠮᠡᠬᠦ ᠳᠠᠷᠤᠮᠠᠯ ᠢ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="85"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="185"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃ </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="186"/>
        <source>Filename</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="190"/>
        <source>Hide scan settings widget</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠵᠤ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Name</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Symbol</source>
        <translation>ᠪᠡᠯᠭᠡᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Type</source>
        <translation>ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>Alert</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ  </translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <source>You need to manually confirm whether the printer devices included in the list are scanning and printing all-in-one machines. If they are not scanning and printing all-in-one machines, ignore them.</source>
        <translation>ᠲᠠ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠳᠦ ᠪᠠᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠪᠠ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠮᠥᠨ ᠡᠰᠡᠬᠦ ᠶᠢ ᠭᠠᠷ ᠢᠶᠠᠷ ᠨᠤᠲᠠᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃ ᠬᠡᠷᠪᠡ ᠲᠡᠳᠡᠭᠡᠷ ᠨᠢ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠪᠠ ᠬᠡᠪᠯᠡᠯ ᠦᠨ ᠪᠦᠬᠦ ᠮᠠᠰᠢᠨ ᠪᠢᠰᠢ ᠪᠣᠯ ᠂ ᠲᠡᠳᠡᠭᠡᠷ ᠢ ᠣᠮᠲᠣᠭᠠᠶᠢᠯᠠᠨᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="71"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="84"/>
        <source>Device List</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠳᠦ ᠵᠢᠭᠰᠠ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="97"/>
        <location filename="../src/newdevicelistpage.cpp" line="117"/>
        <location filename="../src/newdevicelistpage.cpp" line="318"/>
        <location filename="../src/newdevicelistpage.cpp" line="375"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="98"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠨᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="118"/>
        <source>Before</source>
        <translation>ᠥᠩᠭᠡᠷᠡᠭᠰᠡᠨ ᠳᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="119"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="197"/>
        <source>Scanner</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠦᠷ; ᠰᠢᠷᠪᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="199"/>
        <source>Printer</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="216"/>
        <source>Device Name:</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="220"/>
        <source>Driver Name:</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="263"/>
        <source>This driver is from third party, may cause some unmetable result.</source>
        <translation>ᠡᠨᠡ ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠨᠢ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ ᠡᠴᠡ ᠢᠷᠡᠭᠰᠡᠨ ᠶᠤᠮ ᠂ ᠵᠠᠷᠢᠮ ᠬᠡᠮᠵᠢᠵᠦ ᠪᠣᠯᠤᠰᠢ ᠦᠭᠡᠢ ᠦᠷᠡ ᠳ᠋ᠦᠩ ᠢ ᠡᠭᠦᠰᠭᠡᠵᠦ ᠮᠡᠳᠡᠨᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="267"/>
        <source>This driver is provided by the manufacturer. Please contact the corresponding manufacturer of the device to obtain the driver program.</source>
        <translation>ᠡᠨᠡ ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ ᠡᠴᠡ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠶᠤᠮ ᠃ ᠲᠣᠰ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠵᠣᠬᠢᠴᠠᠩᠭᠤᠶ ᠦᠶᠯᠡᠳᠪᠦᠷᠢ ᠲᠡᠶ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠣᠯᠤᠭᠠᠷᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="312"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠫᠷᠦᠭ᠍ᠷᠠᠮ ᠪᠠᠶᠢᠬᠤ ᠦᠭᠡᠢ ᠂ ᠭᠠᠷ ᠢᠶᠠᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠫᠷᠦᠭ᠍ᠷᠠᠮ ᠨᠡᠮᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="316"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="353"/>
        <location filename="../src/newdevicelistpage.cpp" line="458"/>
        <source>Installing driver...</source>
        <translation>ᠶᠠᠭ ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷᠲᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤᠭᠰᠠᠷᠠᠵᠤ ᠁</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="370"/>
        <source>Installation successful. Do you want to use this scanner now? Click &apos;Use&apos; to reset udev (administrator password required) and restart the scanning application to refresh the device list. If you still want to continue operating the scanned image, you will need to manually restart and use the scanning function.</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ ᠨᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠶ ᠃ ᠴᠢ ᠣᠳᠣ ᠡᠨᠡ ᠰᠢᠷᠪᠢᠭᠦᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠸ? 《 Use 》 ᠶᠢ ᠲᠣᠪᠴᠢᠳᠠᠵᠤ udev (ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠰᠡᠨᠡ) ᠢ ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠵᠦ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭ ᠢ ᠰᠢᠨᠡᠳᠬᠡᠨᠡ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠮᠥᠨ ᠯᠡ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠭᠡᠪᠡᠯ ᠂ ᠭᠠᠷ ᠢᠶᠠᠷ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠰᠢᠷᠪᠢᠯᠲᠡ ᠶᠢᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠶ ᠃</translation>
    </message>
    <message>
        <source>Installation successful. Do you want to use this scanner now?</source>
        <translation type="vanished">ᠤᠭᠰᠠᠷᠠᠵᠤ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠠᠢ ᠃ ᠡᠨᠡ ᠰᠢᠷᠦᠭᠦᠷ ᠢ ᠳᠠᠷᠤᠢ᠌ᠬᠠᠨ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="376"/>
        <source>Use</source>
        <translation>ᠬᠡᠷᠡᠭ᠍ᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="407"/>
        <source>Installation failed.</source>
        <translation>ᠢᠯᠠᠭᠳᠠᠯ ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="410"/>
        <source>Ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="452"/>
        <source>Select a directory</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="48"/>
        <location filename="../src/showocrwidget.cpp" line="157"/>
        <location filename="../src/showocrwidget.cpp" line="165"/>
        <source>The document is in character recognition ...</source>
        <translatorcomment>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠪᠣᠯ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠢ ᠢᠯᠭᠠᠨ ᠲᠠᠨᠢᠬᠤ ᠁</translatorcomment>
        <translation>Баримт бичиг нь тэмдэгт таних үе шатанд байна. . .</translation>
    </message>
</context>
</TS>
