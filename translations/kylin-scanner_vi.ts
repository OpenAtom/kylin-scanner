<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="vi">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="33"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="49"/>
        <source>Detect scanners, please waiting</source>
        <translation>Phát hiện máy quét, vui lòng chờ đợi</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="65"/>
        <source>No available scan devices</source>
        <translation>Không có thiết bị quét khả dụng</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="69"/>
        <source>Connect</source>
        <translation>Kết nối</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="46"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="58"/>
        <source>Unable to read text, please retrey</source>
        <translation>Không thể đọc văn bản, vui lòng đọc lại</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>Thoại</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="73"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>Kết nối máy quét, vui lòng nhấp vào nút quét để bắt đầu quét.</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new scanner to refresh the device list.</source>
        <translatorcomment>未检测到扫描仪，请插入新的扫描仪以刷新设备列表。</translatorcomment>
        <translation>Không phát hiện thấy máy quét, hãy cắm máy quét mới để làm mới danh sách thiết bị.</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation type="vanished">麒麟扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="70"/>
        <location filename="../src/mainwidget.cpp" line="170"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="vanished">存在新设备连接，正在重新检测所有扫描设备，请稍等。</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>Single</source>
        <translation type="vanished">单页扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="474"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>Đối số không hợp lệ, vui lòng thay đổi đối số hoặc chuyển đổi các máy quét khác.</translation>
    </message>
    <message>
        <source>error code:</source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="537"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>Quét không thành công, vui lòng kiểm tra máy quét của bạn hoặc chuyển đổi các máy quét khác. Nếu bạn muốn tiếp tục sử dụng máy quét, hãy nhấp vào Tùy chọn, làm mới danh sách để khởi động lại thiết bị.</translation>
    </message>
    <message>
        <source>Detected user manual hibernation. Please check the scanner status. If scanning does not continue, you need to manually click cancel or refresh the list to resume device scanning function.</source>
        <translation type="vanished">检测到用户手动休眠。请检查扫描仪状态。如果扫描无法继续，则需要手动单击“取消”或刷新列表以恢复设备扫描功能。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="619"/>
        <source>Alert</source>
        <translation>Cảnh báo</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="619"/>
        <source>A new Scanner has been connected.</source>
        <translation>Một Máy quét mới đã được kết nối.</translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="172"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="506"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>Thiết bị bận, vui lòng đợi hoặc chuyển đổi các máy quét khác.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="510"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>Khay nạp tài liệu ra khỏi tài liệu, vui lòng đặt giấy và quét lại.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="514"/>
        <source>Scan operation has been cancelled.</source>
        <translation>Hoạt động quét đã bị hủy.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="518"/>
        <source>Scan failed, operation is not supported.</source>
        <translation>Quét không thành công, hoạt động không được hỗ trợ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="521"/>
        <source>Scan failed, Document fedder jammed.</source>
        <translation>Quét không thành công, Bộ nạp tài liệu bị kẹt.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="524"/>
        <source>Scan failed, Error during device I/O.</source>
        <translation>Quét không thành công, Lỗi trong khi I / O thiết bị.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="527"/>
        <source>Scan failed, Out of memory.</source>
        <translation>Quét không thành công, hết bộ nhớ.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="530"/>
        <source>Scan failed, Access to resource has been denied.</source>
        <translation>Quét không thành công, Quyền truy cập vào tài nguyên đã bị từ chối.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="533"/>
        <source>Scan failed, Scanner cover is open.</source>
        <translation>Quét không thành công, Nắp máy quét đang mở.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="584"/>
        <source>System sleep/sleep detected. To ensure normal use of the scanner, please click on restart to restart the scanner application. If you want to continue operating the scanned image, click cancel, but the scanning related functions will be disabled and will take effect after restarting.</source>
        <translation>Đã phát hiện ngủ / ngủ hệ thống. Để đảm bảo sử dụng máy quét bình thường, vui lòng nhấp vào khởi động lại để khởi động lại ứng dụng máy quét. Nếu bạn muốn tiếp tục vận hành hình ảnh đã quét, hãy nhấp vào hủy, nhưng các chức năng liên quan đến quét sẽ bị tắt và sẽ có hiệu lực sau khi khởi động lại.</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="589"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="590"/>
        <source>Restart</source>
        <translation>Khởi động lại</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="660"/>
        <source>Running beauty ...</source>
        <translation>Vẻ đẹp chạy ...</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="682"/>
        <source>Running rectify ...</source>
        <translation>Chạy chỉnh sửa ...</translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="43"/>
        <source>Scanner not detected</source>
        <translation>Máy quét không được phát hiện</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="54"/>
        <location filename="../src/sendmail.cpp" line="77"/>
        <location filename="../src/sendmail.cpp" line="78"/>
        <source>No email client</source>
        <translation>Không có ứng dụng email</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="61"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="89"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>Không tìm thấy ứng dụng email trong hệ thống, vui lòng cài đặt ứng dụng email trước.</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="103"/>
        <source>Install</source>
        <translation>Cài đặt</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="101"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="176"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="235"/>
        <location filename="../src/saneobject.cpp" line="327"/>
        <location filename="../src/saneobject.cpp" line="450"/>
        <location filename="../src/saneobject.cpp" line="511"/>
        <location filename="../src/scansettingswidget.cpp" line="531"/>
        <location filename="../src/scansettingswidget.cpp" line="553"/>
        <source>ADF Duplex</source>
        <translation>ADF Hai mặt</translation>
    </message>
    <message>
        <source>Fail to open the scanner, error code </source>
        <translation type="vanished">打开扫描仪失败，错误代码</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="588"/>
        <source>Fail to open the scanner</source>
        <translation>Không mở được máy quét</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2085"/>
        <source>Auto</source>
        <translation>Xe ô tô</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="450"/>
        <location filename="../src/saneobject.cpp" line="511"/>
        <location filename="../src/scansettingswidget.cpp" line="210"/>
        <location filename="../src/scansettingswidget.cpp" line="531"/>
        <location filename="../src/scansettingswidget.cpp" line="553"/>
        <source>Multiple</source>
        <translation>Nhiều</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="211"/>
        <source>Flatbed</source>
        <translation>Phẳng</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="59"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>Danh sách làm mới. Vui lòng đợi thông tin làm mới thành công...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Scanner is on detecting...</source>
        <translation>Máy quét đang phát hiện...</translation>
    </message>
    <message>
        <location filename="../src/imageBaseOP/savefilebase.cpp" line="235"/>
        <source>Single</source>
        <translation>Đơn</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="287"/>
        <source>Question</source>
        <translation>Câu hỏi</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source>Current </source>
        <translation>Dòng </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source> User</source>
        <translation> Người dùng</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="285"/>
        <source> has already opened kylin-scanner, open will close </source>
        <translation> đã mở kylin-scanner, mở sẽ đóng </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="286"/>
        <source>&apos;s operations. Are you continue?</source>
        <translation>hoạt động của họ. Bạn có tiếp tục không?</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="660"/>
        <source>Color</source>
        <translation>Màu</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="667"/>
        <source>Gray</source>
        <translation>Màu xám</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="673"/>
        <source>Lineart</source>
        <translation>Vẽ đường nét</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="738"/>
        <source>Default Type</source>
        <translation>Loại mặc định</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="817"/>
        <source>Flatbed</source>
        <translation>Phẳng</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="827"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="836"/>
        <source>ADF Front</source>
        <translation>Mặt trước ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="845"/>
        <source>ADF Back</source>
        <translation>ADF Quay lại</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="854"/>
        <source>ADF Duplex</source>
        <translation>ADF Hai mặt</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="904"/>
        <location filename="../src/saneobject.cpp" line="968"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="907"/>
        <location filename="../src/saneobject.cpp" line="972"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="910"/>
        <location filename="../src/saneobject.cpp" line="976"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="913"/>
        <location filename="../src/saneobject.cpp" line="980"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="916"/>
        <location filename="../src/saneobject.cpp" line="984"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="919"/>
        <location filename="../src/saneobject.cpp" line="988"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="922"/>
        <location filename="../src/saneobject.cpp" line="992"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="925"/>
        <location filename="../src/saneobject.cpp" line="996"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="928"/>
        <location filename="../src/saneobject.cpp" line="1000"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="946"/>
        <location filename="../src/saneobject.cpp" line="1016"/>
        <source>Auto</source>
        <translation>Xe ô tô</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <location filename="../src/runningdialog.cpp" line="141"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <location filename="../src/runningdialog.cpp" line="170"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="2011"/>
        <source>Default Type</source>
        <translation>Loại mặc định</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2003"/>
        <source>Flatbed</source>
        <translation>Phẳng</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="416"/>
        <source>Refresh list complete.</source>
        <translation>Làm mới danh sách hoàn tất.</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2005"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2007"/>
        <source>ADF Front</source>
        <translation>Mặt trước ADF</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2009"/>
        <source>ADF Back</source>
        <translation>ADF Quay lại</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2013"/>
        <source>ADF Duplex</source>
        <translation>ADF Hai mặt</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2089"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2091"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2093"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2095"/>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2097"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2099"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2101"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2103"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2105"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="47"/>
        <location filename="../src/scandialog.cpp" line="62"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="66"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="86"/>
        <location filename="../src/scandialog.cpp" line="184"/>
        <source>Number of pages scanning: </source>
        <translation>Số trang quét: </translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="98"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>Hủy quét, vui lòng đợi một chút!</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="182"/>
        <source>Multiple</source>
        <translation>Nhiều</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="769"/>
        <source>Scanner device</source>
        <translation>Thiết bị quét</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="774"/>
        <source>File settings</source>
        <translation>Cài đặt tệp</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="601"/>
        <source>Device</source>
        <translation>Thiết bị</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="126"/>
        <source>Select a directory</source>
        <translation>Chọn một thư mục</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="149"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>Hiện tại người dùng không có quyền sửa đổi thư mục </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="212"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>Chế độ quét phẳng không hỗ trợ quét nhiều lần.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="217"/>
        <location filename="../src/scansettingswidget.cpp" line="967"/>
        <source>Multiple</source>
        <translation>Nhiều</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="299"/>
        <source>This resolution will take a long time to scan, please choose carelly.</source>
        <translation>Độ phân giải này sẽ mất nhiều thời gian để quét, vui lòng chọn cẩn thận.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="300"/>
        <source>Alert</source>
        <translation>Cảnh báo</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="602"/>
        <source>Pages</source>
        <translation>Trang</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="603"/>
        <source>Type</source>
        <translation>Kiểu</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="281"/>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Color</source>
        <translation>Màu</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="605"/>
        <source>Resolution</source>
        <translation>Nghị quyết</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="606"/>
        <source>Size</source>
        <translation>Kích thước</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="607"/>
        <source>Format</source>
        <translation>Định dạng</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="608"/>
        <source>Name</source>
        <translation>Tên</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="777"/>
        <source>scanner01</source>
        <translation>máy quét01</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="609"/>
        <source>Save</source>
        <translation>Cứu</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="617"/>
        <location filename="../src/scansettingswidget.cpp" line="618"/>
        <source>Mail to</source>
        <translation>Gửi thư đến</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="796"/>
        <location filename="../src/scansettingswidget.cpp" line="1148"/>
        <location filename="../src/scansettingswidget.cpp" line="1149"/>
        <source>Save as</source>
        <translation>Lưu dưới dạng</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="214"/>
        <location filename="../src/scansettingswidget.cpp" line="238"/>
        <location filename="../src/scansettingswidget.cpp" line="270"/>
        <location filename="../src/scansettingswidget.cpp" line="967"/>
        <source>Single</source>
        <translation>Đơn</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="295"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="296"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="297"/>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="vanished">该分辨率将会花费很长时间扫描，请谨慎选择！</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="327"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>không thể chứa ký tự &apos;/&apos;.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="332"/>
        <location filename="../src/scansettingswidget.cpp" line="472"/>
        <source>cannot save as hidden file.</source>
        <translation>không thể lưu dưới dạng tệp ẩn.</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="461"/>
        <source>Save As</source>
        <translation>Lưu dưới dạng</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="485"/>
        <source>Path without access rights: </source>
        <translation>Đường dẫn không có quyền truy cập: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="489"/>
        <source>File path that does not exist: </source>
        <translation>Đường dẫn tệp không tồn tại: </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="548"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation> đã tồn tại, bạn có muốn ghi đè lên nó không? Nếu bạn đang thực hiện quét nhiều trang, nó có thể khiến nhiều tệp bị ghi đè. Hãy thận trọng!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="746"/>
        <source>Start Scan</source>
        <translation>Bắt đầu quét</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1139"/>
        <location filename="../src/scansettingswidget.cpp" line="1140"/>
        <source>Store text</source>
        <translation>Lưu trữ văn bản</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="547"/>
        <source>The file </source>
        <translation>Tập tin </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="vanished">已存在，您想覆盖它吗？</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="550"/>
        <source>tips</source>
        <translation>Mẹo</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="604"/>
        <source>Colour</source>
        <translation>Màu</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="269"/>
        <location filename="../src/scansettingswidget.cpp" line="984"/>
        <source>Flatbed</source>
        <translation>Phẳng</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="984"/>
        <source>ADF</source>
        <translation>ADF</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Gray</source>
        <translation>Màu xám</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="283"/>
        <location filename="../src/scansettingswidget.cpp" line="1013"/>
        <source>Lineart</source>
        <translation>Vẽ đường nét</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1036"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1053"/>
        <source>Resolution is empty!</source>
        <translation>Độ phân giải trống rỗng!</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1068"/>
        <source>A4</source>
        <translation>Đáp 4</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1068"/>
        <source>A5</source>
        <translation>Đáp 5</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1365"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1368"/>
        <source>Yes</source>
        <translation>Có</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="198"/>
        <location filename="../src/sendmail.cpp" line="222"/>
        <source>Select email client</source>
        <translation>Chọn ứng dụng email</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="204"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="207"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="236"/>
        <location filename="../src/sendmail.cpp" line="237"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="240"/>
        <location filename="../src/sendmail.cpp" line="241"/>
        <source>Confirm</source>
        <translation>Xác nhận</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="66"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="74"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1148"/>
        <source>Canceling...Please waiting...</source>
        <translation>Huỷ... Xin hãy chờ đợi...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1159"/>
        <source>Running beauty ...</source>
        <translation>Vẻ đẹp chạy ...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1192"/>
        <source>Running rectify ...</source>
        <translation>Chạy chỉnh sửa ...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>Dạng</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>máy quét kylin</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="194"/>
        <location filename="../src/titlebar/titlebar.h" line="83"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>Option</source>
        <translation>Sự quyết định</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="179"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="220"/>
        <source>Minimize</source>
        <translation>Giảm thiểu</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="261"/>
        <location filename="../src/titlebar/titlebar.cpp" line="145"/>
        <source>Maximize</source>
        <translation>Tối đa hóa</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="308"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="53"/>
        <source>Refresh List</source>
        <translation>Danh sách làm mới</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="70"/>
        <source>Help</source>
        <translation>Trợ giúp</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="74"/>
        <source>About</source>
        <translation>Về</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="76"/>
        <source>Version: </source>
        <translation>Phiên bản: </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="77"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>Tin nhắn cung cấp trò chuyện văn bản và các chức năng truyền tệp trong mạng LAN. Không cần phải xây dựng một máy chủ. Nó hỗ trợ nhiều người tương tác cùng một lúc và gửi và nhận song song.</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="87"/>
        <source>Exit</source>
        <translation>Thoát</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="142"/>
        <source>Restore</source>
        <translation>Khôi phục</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="192"/>
        <source>The current file is not saved. Do you want to save it?</source>
        <translation>Tệp hiện tại không được lưu. Bạn có muốn lưu nó không?</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="vanished">当前文档未保存，是否保存？</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="196"/>
        <source>Straight &amp;Exit</source>
        <translation>Thẳng &amp; Thoát</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="197"/>
        <source>&amp;Save Exit</source>
        <translation>&amp;Lưu Thoát</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="64"/>
        <location filename="../src/toolbarwidget.cpp" line="158"/>
        <source>Beauty</source>
        <translation>Vẻ đẹp</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="70"/>
        <location filename="../src/toolbarwidget.cpp" line="164"/>
        <source>Rectify</source>
        <translation>Khắc phục</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="76"/>
        <location filename="../src/toolbarwidget.cpp" line="170"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="84"/>
        <location filename="../src/toolbarwidget.cpp" line="178"/>
        <source>Crop</source>
        <translation>Cây trồng</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="90"/>
        <location filename="../src/toolbarwidget.cpp" line="184"/>
        <source>Rotate</source>
        <translation>Xoay</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="96"/>
        <location filename="../src/toolbarwidget.cpp" line="190"/>
        <source>Mirror</source>
        <translation>Gương</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="102"/>
        <location filename="../src/toolbarwidget.cpp" line="196"/>
        <source>Watermark</source>
        <translation>Watermark</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="121"/>
        <location filename="../src/toolbarwidget.cpp" line="215"/>
        <source>ZoomIn</source>
        <translation>Phóng to</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="115"/>
        <location filename="../src/toolbarwidget.cpp" line="209"/>
        <source>ZoomOut</source>
        <translation>Thu nhỏ</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation type="vanished">扫描仪已断开连接！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="95"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>Truy vấn thiết bị máy quét. Xin hãy chờ đợi...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="119"/>
        <source>New Scanner has been Connected.</source>
        <translation>Máy quét mới đã được kết nối.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="129"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>Máy quét bị ngắt kết nối. Nếu bạn ngắt kết nối máy quét đang quét, hãy nhấp vào Hủy quét hoặc đợi máy quét báo thông báo lỗi.</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="135"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>Máy quét bị ngắt kết nối, làm mới danh sách máy quét. Xin hãy chờ đợi...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="150"/>
        <source>Scanner list refresh complete.</source>
        <translation>Hoàn tất làm mới danh sách máy quét.</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="46"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="59"/>
        <source>Searching for scanner...</source>
        <translation>Đang tìm kiếm máy quét...</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="46"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="54"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="65"/>
        <source>Add watermark</source>
        <translation>Thêm hình mờ</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="81"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>Xác nhận</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="200"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>Mở tập tin &lt;filename&gt;</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Filename</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="205"/>
        <source>Hide scan settings widget</source>
        <translation>Ẩn tiện ích cài đặt quét</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Name</source>
        <translation>Tên</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Symbol</source>
        <translation>Biểu tượng</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="54"/>
        <source>Type</source>
        <translation>Kiểu</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <location filename="../src/newdevicelistpage.cpp" line="255"/>
        <location filename="../src/newdevicelistpage.cpp" line="259"/>
        <source>Alert</source>
        <translation>Cảnh báo</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="62"/>
        <source>You need to manually confirm whether the printer devices included in the list are scanning and printing all-in-one machines. If they are not scanning and printing all-in-one machines, ignore them.</source>
        <translation>Bạn cần xác nhận thủ công xem các thiết bị máy in có trong danh sách có đang quét và in máy tất cả trong một hay không. Nếu họ không quét và in máy tất cả trong một, hãy bỏ qua chúng.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="71"/>
        <source>Close</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="84"/>
        <source>Device List</source>
        <translation>Danh sách thiết bị</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="97"/>
        <location filename="../src/newdevicelistpage.cpp" line="117"/>
        <location filename="../src/newdevicelistpage.cpp" line="309"/>
        <location filename="../src/newdevicelistpage.cpp" line="366"/>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="98"/>
        <source>Next</source>
        <translation>Sau</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="118"/>
        <source>Before</source>
        <translation>Trước</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="119"/>
        <source>Install</source>
        <translation>Cài đặt</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="189"/>
        <source>Scanner</source>
        <translation>Máy quét</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="191"/>
        <source>Printer</source>
        <translation>Máy in</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="208"/>
        <source>Device Name:</source>
        <translation>Tên thiết bị:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="212"/>
        <source>Driver Name:</source>
        <translation>Tên tài xế:</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="255"/>
        <source>This driver is from third party, may cause some unmetable result.</source>
        <translation>Trình điều khiển này đến từ bên thứ ba, có thể gây ra một số kết quả không thể đo được.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="259"/>
        <source>This driver is provided by the manufacturer. Please contact the corresponding manufacturer of the device to obtain the driver program.</source>
        <translation>Trình điều khiển này được cung cấp bởi nhà sản xuất. Vui lòng liên hệ với nhà sản xuất thiết bị tương ứng để có được chương trình trình điều khiển.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="303"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>Không có trình điều khiển khả dụng, bạn có muốn thêm trình điều khiển theo cách thủ công không?</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="307"/>
        <source>Add</source>
        <translation>Thêm</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="344"/>
        <location filename="../src/newdevicelistpage.cpp" line="449"/>
        <source>Installing driver...</source>
        <translation>Cài đặt trình điều khiển...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="361"/>
        <source>Installation successful. Do you want to use this scanner now? Click &apos;Use&apos; to reset udev (administrator password required) and restart the scanning application to refresh the device list. If you still want to continue operating the scanned image, you will need to manually restart and use the scanning function.</source>
        <translation>Cài đặt thành công. Bạn có muốn sử dụng máy quét này ngay bây giờ không? Nhấp vào &apos;Sử dụng&apos; để đặt lại udev (yêu cầu mật khẩu quản trị viên) và khởi động lại ứng dụng quét để làm mới danh sách thiết bị. Nếu bạn vẫn muốn tiếp tục vận hành hình ảnh đã quét, bạn sẽ cần khởi động lại thủ công và sử dụng chức năng quét.</translation>
    </message>
    <message>
        <source>Installation successful. Do you want to use this scanner now?</source>
        <translation type="vanished">安装成功。你想现在使用这个扫描仪吗？</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="367"/>
        <source>Use</source>
        <translation>Dùng</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="398"/>
        <source>Installation failed.</source>
        <translation>Cài đặt không thành công.</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="401"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="443"/>
        <source>Select a directory</source>
        <translation>Chọn một thư mục</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="48"/>
        <location filename="../src/showocrwidget.cpp" line="157"/>
        <location filename="../src/showocrwidget.cpp" line="165"/>
        <source>The document is in character recognition ...</source>
        <translation>Tài liệu đang trong nhận dạng ký tự ...</translation>
    </message>
</context>
</TS>
