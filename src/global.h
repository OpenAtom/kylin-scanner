/*
* Copyright (C) 2021, KylinSoft Co., Ltd.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, see <http://www.gnu.org/licenses/&gt;.
*
*/
#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <QString>
#include <QSettings>


class QSettings;

namespace Global {

enum KERROR_TYPE{
    NO_ERROR = 0
};

//! Read and store application settings
extern QSettings        *g_settings;


void global_init();
void global_end();
}

#endif

