#ifndef CREATOFD_H
#define CREATOFD_H

#include <QString>
#include <QList>

struct zip;
class OFD
{
public:
    OFD(){}
    static bool createOFD(QString fileName, QStringList imageList);
    static void setOFDTitle(QString value);
    static void setOFDCreator(QString value);
private:
    static void addFileFromData(QString fileName, QByteArray data);
    static void addFileFromFile(QString fileName, QString origFileName);
    static QString getMaxIDAndAdd();
    static void clearImageData();
    static QString m_OFD;
    static QString m_OFDDoc;
    static QString m_OFDContent;
    static QString m_OFDPublicRes;
    static QString m_docTitle;
    static QString m_docCreator;
    static zip *m_zipHandle;
    static int m_docID;
    static QList<char *> m_imageData;
};

#endif // CREATOFD_H
