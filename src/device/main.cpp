#include <iostream>
#include <QCoreApplication>
#include "deviceFinder.h"
#include "ukui_apt.h"



void testfind()
{
    deviceFinder *f = new deviceFinder;
    QObject::connect(f, &deviceFinder::succeed, [=]() {
        qDebug() << "Install succeed";
        qDebug() << f->getList();
        f->finished();
    });
    QObject::connect(f, &deviceFinder::failed, [=]() {
        qDebug() << "Install failed";
    });
    f->startWorker();
}

void testapt() {
    AptUtilHelper* aptHelper = new AptUtilHelper(DeviceInformation());
    QObject::connect(aptHelper, &AptUtilHelper::succeed, [=]() {
        qDebug() << "Install succeed";
    });
    QObject::connect(aptHelper, &AptUtilHelper::failed, [=]() {
        qDebug() << "Install failed";
    });
    aptHelper->startWorker();
}

void testdeb() {
    DebUtilHelper* debHelper = new DebUtilHelper("brscan5chn_1.2.5-0_amd64.deb");
    QObject::connect(debHelper, &DebUtilHelper::succeed, [=]() {
        qDebug() << "Install succeed";

    });
    QObject::connect(debHelper, &DebUtilHelper::failed, [=]() {
        qDebug() << "Install failed";
    });
    debHelper->startWorker();
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);


    testfind();
//    testapt();
//    testdeb();

    return app.exec();
}
